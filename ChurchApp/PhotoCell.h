//
//  PhotoCell.h
//  ChurchApp
//
//  Created by Grigo Mathews on 27/02/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *CardView;
@property (weak, nonatomic) IBOutlet UIImageView *photoImage;

@end
