//
//  ContactsViewController.m
//  ChurchApp
//
//  Created by Grigo Mathews on 31/01/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import "ContactsViewController.h"
#import "ContactCell.h"
#import "UIColor+GlobalColor.h"
#import "GlobalIdentifiers.h"
#import <MessageUI/MessageUI.h>
#import <MapKit/MapKit.h>

@interface ContactsViewController ()<UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate>
{
	NSArray *ChurchContactList;
	NSArray *AdminContactList;
	NSArray *TrusteeList;
	NSArray *ParishCouncilMemberList;
	NSArray *CongregationList;
	NSArray *SacristyList;
	NSArray *EducationalList;
	NSArray *PoweredBy;
}
@property (weak, nonatomic) IBOutlet UITableView *ContactTableView;

- (IBAction)BackBtnAction:(id)sender;

@end

@implementation ContactsViewController
@synthesize ContactTableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	ChurchContactList = [NSArray arrayWithObjects:@"Marth Mariam Forane Church\nKuravilangad Post, Kottayam Dist,\nKerala, India", @"Location in map", @"Call parish office", @"Email us", @"Visit parish website", @"Visit Facebook page",nil];
	
//	NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//	NSString *documentFolder = [path objectAtIndex:0];
//	NSString *filePath	= [documentFolder stringByAppendingFormat:@"/Contact.plist"];
//	NSDictionary *FullListArray = [NSDictionary dictionaryWithContentsOfFile:filePath];
	
	NSDictionary *mainDictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Contact" ofType:@"plist"]];
	
	AdminContactList		= [mainDictionary objectForKey:@"Adminstration"];
	TrusteeList				= [mainDictionary objectForKey:@"Trustee"];
	ParishCouncilMemberList = [mainDictionary objectForKey:@"Parish Concil Members"];
	CongregationList		= [mainDictionary objectForKey:@"Congregations"];
	SacristyList			= [mainDictionary objectForKey:@"Sacristy"];
	EducationalList			= [mainDictionary objectForKey:@"Educational Institutions"];
	PoweredBy				= [mainDictionary objectForKey:@"Powered By"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)BackBtnAction:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UItableview 
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 8;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	switch (section) {
		case 0:
			return [ChurchContactList count];//6
			break;
		case 1:
			return [AdminContactList count];//6
			break;
		case 2:
			return [TrusteeList count];//6
			break;
		case 3:
			return [ParishCouncilMemberList count]; //53
			break;
		case 4:
			return [CongregationList count]; //11
			break;
		case 5:
			return [SacristyList count]; //3
			break;
		case 6:
			return [EducationalList count]; //7
			break;
		case 7:
			return [PoweredBy count]; //1
			break;
		default:
			return 0;
			break;
	}
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 81;
}


- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	switch (section) {//Trustee
		case 0:
			return @"The Church";
			break;
		case 1:
			return @"Administration";
			break;
		case 2:
			return @"Trustee";
			break;
		case 3:
			return @"Parish Council Members";
			break;
		case 4:
			return @"Congregations";
			break;
		case 5:
			return @"Sacristy";
			break;
		case 6:
			return @"Educational Instructions";
			break;
		case 7:
			return @"Powered by";
			break;
		default:
			return @"";
			break;
	}
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	return 30;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	
	UILabel *myLabel	= [[UILabel alloc] init];
	myLabel.frame		= CGRectMake(10, 1, 667, 30);
	myLabel.font		= [UIFont fontWithName:@"Signika-Bold" size:15.0];
	myLabel.textColor	= [UIColor whiteColor];
	myLabel.text		= [self tableView:tableView titleForHeaderInSection:section];
	
	UIView *headerView	= [[UIView alloc] init];
	headerView.backgroundColor = [UIColor headerBgColor];
	headerView.layer.cornerRadius = 5.0f;
	[headerView addSubview:myLabel];
	
	return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *cellIdentifier = @"cellIdentifier";
	static NSString *cellId1 = @"ContactCell";
	
	if (indexPath.section == 0) {
		if (indexPath.row == 0) {
			UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
			if (cell == nil) {
				cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
				cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
				cell.textLabel.numberOfLines = 0;
				cell.textLabel.font = [UIFont fontWithName:@"Signika-Regular" size:17.0f];
			}
			cell.backgroundColor = [UIColor clearColor];
			cell.textLabel.text = [ChurchContactList objectAtIndex:indexPath.row];
			return cell;
		}else {
			ContactCell *hCell = (ContactCell *)[tableView dequeueReusableCellWithIdentifier:cellId1];
			if(hCell == nil) {
				NSArray *customObjects = [[NSBundle mainBundle]loadNibNamed:@"ContactCell" owner:self options:nil];
				for(id objects in customObjects){
					if([objects isKindOfClass:[UITableViewCell class]]) {
						hCell = (ContactCell *)objects;
					}
				}
			}
			hCell.Lbl1.textAlignment = 1;
			hCell.Lbl1.text = [ChurchContactList objectAtIndex:indexPath.row];
			return hCell;
		}
	} else {
		ContactCell *hCell = (ContactCell *)[tableView dequeueReusableCellWithIdentifier:cellId1];
		if(hCell == nil) {
			NSArray *customObjects = [[NSBundle mainBundle]loadNibNamed:@"ContactCell" owner:self options:nil];
			for(id objects in customObjects){
				if([objects isKindOfClass:[UITableViewCell class]]) {
					hCell = (ContactCell *)objects;
					
				}
			}
		}
		hCell.Lbl1.textAlignment = 0;
		if (indexPath.section == 1) {
			hCell.Lbl1.text = [NSString stringWithFormat:@"%@\n%@",[[AdminContactList objectAtIndex:indexPath.row] objectForKey:@"Name"],[[AdminContactList objectAtIndex:indexPath.row] objectForKey:@"Mobile"]];
		}else if (indexPath.section == 2) {
			hCell.Lbl1.text = [NSString stringWithFormat:@"%@\n%@",[[TrusteeList objectAtIndex:indexPath.row] objectForKey:@"Name"],[[TrusteeList objectAtIndex:indexPath.row] objectForKey:@"Mobile"]];
		}else if (indexPath.section == 3) {
			hCell.Lbl1.text = [NSString stringWithFormat:@"%@\n%@",[[ParishCouncilMemberList objectAtIndex:indexPath.row] objectForKey:@"Name"],[[ParishCouncilMemberList objectAtIndex:indexPath.row] objectForKey:@"Mobile"]];
		}else if (indexPath.section == 3) {
			hCell.Lbl1.text = [NSString stringWithFormat:@"%@\n%@",[[CongregationList objectAtIndex:indexPath.row] objectForKey:@"Name"],[[CongregationList objectAtIndex:indexPath.row] objectForKey:@"Mobile"]];
		}else if (indexPath.section == 4) {
			hCell.Lbl1.text = [NSString stringWithFormat:@"%@\n%@",[[SacristyList objectAtIndex:indexPath.row] objectForKey:@"Name"],[[SacristyList objectAtIndex:indexPath.row] objectForKey:@"Mobile"]];
		}else if (indexPath.section == 5) {
			hCell.Lbl1.text = [NSString stringWithFormat:@"%@\n%@",[[EducationalList objectAtIndex:indexPath.row] objectForKey:@"Name"],[[EducationalList objectAtIndex:indexPath.row] objectForKey:@"Mobile"]];
		}else if (indexPath.section == 6) {
			hCell.Lbl1.text = [NSString stringWithFormat:@"%@\n%@",[[PoweredBy objectAtIndex:indexPath.row] objectForKey:@"Name"],[[PoweredBy objectAtIndex:indexPath.row] objectForKey:@"Mobile"]];
		}else {
			hCell.Lbl1.text = @"";
		}
		return hCell;
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if (indexPath.section == 0) {
		if (indexPath.row == 1) {
			CLLocationCoordinate2D endingCoord = CLLocationCoordinate2DMake(9.754400, 76.565000);
			MKPlacemark *endLocation = [[MKPlacemark alloc] initWithCoordinate:endingCoord addressDictionary:nil];
			MKMapItem *endingItem = [[MKMapItem alloc] initWithPlacemark:endLocation];
			NSMutableDictionary *launchOptions = [[NSMutableDictionary alloc] init];
			[endingItem openInMapsWithLaunchOptions:launchOptions];
		}else if (indexPath.row == 2) {
			[[UIApplication sharedApplication] openURL:[NSURL URLWithString:[@"tel://" stringByAppendingString:@"04822230224"]]];
		}else if (indexPath.row == 3) {
			if ([MFMailComposeViewController canSendMail]) {
				MFMailComposeViewController *composeViewController = [[MFMailComposeViewController alloc] initWithNibName:nil bundle:nil];
				[composeViewController setMailComposeDelegate:self];
				[composeViewController setToRecipients:@[@"kuravilangadchurch@gmail.com"]];
				[composeViewController setSubject:@"Message"];
				[self presentViewController:composeViewController animated:YES completion:nil];
			}
		}else if (indexPath.row == 4) {
			[[UIApplication sharedApplication] openURL:[NSURL URLWithString:kChurchWesiteURL]];
		}else if (indexPath.row == 5) {
			[[UIApplication sharedApplication] openURL:[NSURL URLWithString:kFacebookURL]];
		}
	}else {
		if (indexPath.section == 1) {
			[self SelectingPhoneNumber:[[[AdminContactList objectAtIndex:indexPath.row] objectForKey:@"Mobile"] componentsSeparatedByString:@"/"]];
		}else if (indexPath.section == 2) {
			[self SelectingPhoneNumber:[[[TrusteeList objectAtIndex:indexPath.row] objectForKey:@"Mobile"] componentsSeparatedByString:@"/"]];
		}else if (indexPath.section == 3) {
			[self SelectingPhoneNumber:[[[ParishCouncilMemberList objectAtIndex:indexPath.row] objectForKey:@"Mobile"] componentsSeparatedByString:@"/"]];
		}else if (indexPath.section == 4) {
			[self SelectingPhoneNumber:[[[CongregationList objectAtIndex:indexPath.row] objectForKey:@"Mobile"] componentsSeparatedByString:@"/"]];
		}else if (indexPath.section == 5) {
			[self SelectingPhoneNumber:[[[SacristyList objectAtIndex:indexPath.row] objectForKey:@"Mobile"] componentsSeparatedByString:@"/"]];
		}else if (indexPath.section == 6) {
			[self SelectingPhoneNumber:[[[EducationalList objectAtIndex:indexPath.row] objectForKey:@"Mobile"] componentsSeparatedByString:@"/"]];
		}else if (indexPath.section == 7) {
			[self SelectingPhoneNumber:[[[PoweredBy objectAtIndex:indexPath.row] objectForKey:@"Mobile"] componentsSeparatedByString:@"/"]];
		}
	}
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)SelectingPhoneNumber:(NSArray *)arr {
	UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Select number to call" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
	[actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
	}]];
	if ([arr count] == 1) {
		[actionSheet addAction:[UIAlertAction actionWithTitle:arr[0] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
			[self CallPhone:arr[0]];
		}]];
	}else if ([arr count] == 2) {
		[actionSheet addAction:[UIAlertAction actionWithTitle:arr[0] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
			[self CallPhone:arr[0]];
		}]];
		[actionSheet addAction:[UIAlertAction actionWithTitle:arr[1] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
			[self CallPhone:arr[1]];
		}]];
	}else if ([arr count] == 3) {
		[actionSheet addAction:[UIAlertAction actionWithTitle:arr[0] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
			[self CallPhone:arr[0]];
		}]];
		[actionSheet addAction:[UIAlertAction actionWithTitle:arr[1] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
			[self CallPhone:arr[1]];
		}]];
		[actionSheet addAction:[UIAlertAction actionWithTitle:arr[2] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
			[self CallPhone:arr[2]];
		}]];
	}
	[self presentViewController:actionSheet animated:YES completion:nil];
}


- (void)CallPhone:(NSString *)phNo {
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:[@"tel://" stringByAppendingString:phNo]]];
}

#pragma mark - MFMailComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
	//Add an alert in case of failure
	[self dismissViewControllerAnimated:YES completion:nil];
}

@end
