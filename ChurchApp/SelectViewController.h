//
//  SelectViewController.h
//  ChurchApp
//
//  Created by Grigo Mathews on 24/02/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OfferingsViewController.h"


@interface SelectViewController : UIViewController

@property (strong, nonatomic) id <OfferingsDelegate> delegateOD;

@end
