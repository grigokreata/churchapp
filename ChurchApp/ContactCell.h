//
//  ContactCell.h
//  ChurchApp
//
//  Created by Grigo Mathews on 08/02/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *CardView;
@property (weak, nonatomic) IBOutlet UILabel *Lbl1;

@end
