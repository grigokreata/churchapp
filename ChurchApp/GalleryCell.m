//
//  GalleryCell.m
//  ChurchApp
//
//  Created by Grigo Mathews on 09/02/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import "GalleryCell.h"
#import "UIColor+GlobalColor.h"

@implementation GalleryCell
@synthesize CardView;

- (void)awakeFromNib {
	[super awakeFromNib];
	CardView.backgroundColor		= [UIColor cardBgColor];
	CardView.layer.shadowColor		= [UIColor grayColor].CGColor;
	CardView.layer.shadowOffset		= CGSizeMake(0, 1);
	CardView.layer.shadowOpacity	= 1;
	CardView.layer.shadowRadius		= 1.0;
	CardView.layer.cornerRadius		= 2.0;
	CardView.clipsToBounds			= NO;
}

@end
