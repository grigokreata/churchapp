//
//  SelectViewController.m
//  ChurchApp
//
//  Created by Grigo Mathews on 24/02/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import "SelectViewController.h"

@interface SelectViewController ()<UITableViewDataSource, UITableViewDelegate>
{
	NSArray *ListArray;
	NSMutableArray *SelMalArr;
	NSMutableArray *SelRateArr;
}

@property (weak, nonatomic) IBOutlet UITableView *SelectTableView;

- (IBAction)BackBtnAction:(id)sender;
- (IBAction)DoneBtnAction:(id)sender;

@end

@implementation SelectViewController
@synthesize SelectTableView;
@synthesize delegateOD;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	SelMalArr	= [[NSMutableArray alloc] init];
	SelRateArr	= [[NSMutableArray alloc] init];
	ListArray	= [NSArray arrayWithObjects:
				 @{@"rate": @"75",	@"title": @"സാധാരണ കുർബാന - Rs. 75 - Ordinary Holy Mass", @"mal": @"സാധാരണ കുർബാന"},
				 @{@"rate": @"150", @"title": @"ആഘോഷമായ പാട്ടുകുർബാന - Rs. 150 - Solemn Holy Mass", @"mal": @"ആഘോഷമായ പാട്ടുകുർബാന"},
				 @{@"rate": @"225", @"title": @"കൂടുതുറന്ന ആഘോഷമായ പാട്ടുകുർബാന - Rs. 225 - Most Solemnly Holymass", @"mal": @"കൂടുതുറന്ന ആഘോഷമായ പാട്ടുകുർബാന"},
				 @{@"rate": @"60",	@"title": @"മുത്തിയമ്മയുടെ നൊവേന - Rs. 60 - Novena", @"mal": @"മുത്തിയമ്മയുടെ നൊവേന"},
				 @{@"rate": @"100", @"title": @"ലദീഞ്ഞ് - Rs. 100 - Latheenju", @"mal": @"ലദീഞ്ഞ്"},
				 @{@"rate": @"20",	@"title": @"അടിമ - Rs. 20 - Adima", @"mal": @"അടിമ"},
				 @{@"rate": @"20",	@"title": @"കഴുന്ന് / മുടി - Rs. 20 - Kazhunnu, Mudi", @"mal": @"കഴുന്ന് / മുടി"},
				 @{@"rate": @"10",	@"title": @"നേർച്ച വസ്തുക്കൾ - Rs. 10 - Nercha Vasthukal", @"mal": @"നേർച്ച വസ്തുക്കൾ"}, nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)BackBtnAction:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)DoneBtnAction:(id)sender {
	NSLog(@"SelMalArr: %@",SelMalArr);
	NSLog(@"SelRateArr: %@",SelRateArr);
	if (delegateOD) {
		[delegateOD getSelectedArray:SelRateArr MalArray:SelMalArr];
		[self BackBtnAction:self];
	}
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [ListArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *cellIdentifier = @"cellIdentifier";
	
	UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	if (cell == nil) {
		cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
	}
	cell.detailTextLabel.font	 = [UIFont fontWithName:@"Signika-Semibold" size:15.0];
	cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
	cell.textLabel.numberOfLines = 0;
	cell.textLabel.text = [[ListArray objectAtIndex:indexPath.row] objectForKey:@"title"];
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
		cell.accessoryType = UITableViewCellAccessoryNone;
		[SelMalArr removeObject:[[ListArray objectAtIndex:indexPath.row] objectForKey:@"mal"]];
		[SelRateArr removeObject:[[ListArray objectAtIndex:indexPath.row] objectForKey:@"rate"]];
	}else {
		cell.accessoryType = UITableViewCellAccessoryCheckmark;
		[SelMalArr addObject:[[ListArray objectAtIndex:indexPath.row] objectForKey:@"mal"]];
		[SelRateArr addObject:[[ListArray objectAtIndex:indexPath.row] objectForKey:@"rate"]];
	}
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
