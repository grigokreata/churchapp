//
//  HBCListCell.h
//  ChurchApp
//
//  Created by Grigo Mathews on 13/02/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HBCListCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *CardView;
@property (weak, nonatomic) IBOutlet UILabel *hbclbl1;
@end
