//
//  NewsCell.m
//  ChurchApp
//
//  Created by Grigo Mathews on 01/02/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import "NewsCell.h"
#import "UIColor+GlobalColor.h"

@implementation NewsCell
@synthesize CardView;
@synthesize NewsLbl1;
@synthesize NewsImage;


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	CardView.backgroundColor = [UIColor cardBgColor];
	CardView.layer.shadowColor = [UIColor grayColor].CGColor;
	CardView.layer.shadowOffset = CGSizeMake(0, 1);
	CardView.layer.shadowOpacity = 1;
	CardView.layer.shadowRadius = 1.0;
	CardView.layer.cornerRadius = 2.0;
	CardView.clipsToBounds = NO;
	
//	NewsImage.image = [UIImage imageNamed:@"mary"];
//	NewsImage.backgroundColor = [UIColor headerBgColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
