//
//  NewsDetailViewController.h
//  ChurchApp
//
//  Created by Grigo Mathews on 24/02/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsDetailViewController : UIViewController

@property (strong, nonatomic) NSDictionary *passingDic;

@end
