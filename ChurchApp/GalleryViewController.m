//
//  GalleryViewController.m
//  ChurchApp
//
//  Created by Grigo Mathews on 31/01/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import "GalleryViewController.h"
#import "ServerClass.h"
#import "GalleryCell.h"
#import "UIImageView+WebCache.h"
#import "GlobalIdentifiers.h"
#import "VideoCell.h"
//#import "AlbumViewController.h"

static NSString *GalleryCellId = @"GalleryCell";

@interface GalleryViewController ()<ServerClassDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDelegate, UITableViewDataSource>
{
	ServerClass *serverclass;
	NSMutableArray *GalleryListArray;
	NSMutableArray *VideoListArray;
	NSString *sVideoURL;
	NSString *AlbumId;
	long APINo;
}
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UICollectionView *GalleryCollectionView;
@property (weak, nonatomic) IBOutlet UITableView *VideoTableView;
@property (weak, nonatomic) IBOutlet UIWebView *LiveWebView;

- (IBAction)BackBtnAction:(id)sender;

@end

@implementation GalleryViewController
@synthesize delegateGD;
@synthesize passingNo; // Gallery=1, Video=2, Live=3.
@synthesize indicator;
@synthesize GalleryCollectionView;
@synthesize VideoTableView;
@synthesize LiveWebView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	serverclass = [[ServerClass alloc] init];
	GalleryListArray = [[NSMutableArray alloc] init];
	VideoListArray   = [[NSMutableArray alloc] init];
	[self processEnd];
	if (passingNo == 1) { // gallery
		GalleryCollectionView.hidden = NO;
		VideoTableView.hidden = YES;
		LiveWebView.hidden = YES;
		[self ServerRequestGallery];
	}else if (passingNo == 2) { //video
		GalleryCollectionView.hidden = YES;
		VideoTableView.hidden = NO;
		LiveWebView.hidden = YES;
		[self ServerRequestVideo];
	}else { // go live
		GalleryCollectionView.hidden = YES;
		VideoTableView.hidden = YES;
		LiveWebView.hidden = NO;
		//Load the request in the UIWebView.
		[LiveWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:kLiveChurchURL]]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)BackBtnAction:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
	return [GalleryListArray count];
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath  {
	
	GalleryCell *gCell	= (GalleryCell *)[collectionView dequeueReusableCellWithReuseIdentifier:GalleryCellId forIndexPath:indexPath];
	NSURL *gImageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[[GalleryListArray objectAtIndex:indexPath.row] objectForKey:@"image"]]];
	[gCell.gImageView sd_setImageWithURL:gImageUrl placeholderImage:[UIImage imageNamed:@"cross"]];
	gCell.gTitle.text = [[GalleryListArray objectAtIndex:indexPath.row] objectForKey:@"albumtitle"];
	return gCell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
	CGFloat picDimension_width  = (self.view.frame.size.width / 2.0f) - 15.0f;
	CGFloat picDimension_height = (self.view.frame.size.height / 3.0f) + 15.0f;
	NSLog(@" picDimension_width  = %f",picDimension_width);
//	NSLog(@" picDimension_height = %f",picDimension_height);
//	return CGSizeMake(151, 178);
	return CGSizeMake(picDimension_width, picDimension_height);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
	return UIEdgeInsetsMake(10, 10, 10, 10);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath  {
	NSLog(@"didSelectItemAtIndexPath: %@", [GalleryListArray objectAtIndex:indexPath.row]);
	if (delegateGD) {
		[delegateGD SelectedGalleryRowAtIndexPath:[[GalleryListArray objectAtIndex:indexPath.row] objectForKey:@"id"] AlbumName:[[GalleryListArray objectAtIndex:indexPath.row] objectForKey:@"albumtitle"]];
	}
}

#pragma mark-  UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (section == 0) {
		if ([VideoListArray count]) {
			return 1;
		}else {
			return 0;
		}
	}else {
		return [VideoListArray count];
	}
	
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	switch (indexPath.section) {
		case 0:
			return 250;
			break;
		case 1:
			return 44;
			break;
		default:
			return 0;
			break;
	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *cellIdentifier = @"cellIdentifier";
	static NSString *CellId1 = @"VideoCell";
	
	if (indexPath.section == 0) {
		VideoCell *vCell = (VideoCell *)[tableView dequeueReusableCellWithIdentifier:CellId1];
		if(vCell == nil) {
			NSArray *customObjects = [[NSBundle mainBundle]loadNibNamed:@"VideoCell" owner:self options:nil];
			for(id objects in customObjects){
				if([objects isKindOfClass:[UITableViewCell class]]) {
					vCell = (VideoCell *)objects;
				}
			}
		}
		vCell.VideoWebView.scrollView.scrollEnabled = NO;
		vCell.VideoWebView.scrollView.bounces = NO;
		if (sVideoURL == nil) {
			sVideoURL = [[VideoListArray objectAtIndex:indexPath.row] objectForKey:@"videotitle"];
		}
		[vCell.VideoWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:sVideoURL]]];
		return vCell;
	}else {
		UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
		if(cell == nil) {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
			cell.detailTextLabel.font			= [UIFont fontWithName:@"Signika-Semibold" size:15.0];
		}
		cell.textLabel.text = [[VideoListArray objectAtIndex:indexPath.row] objectForKey:@"videotitle"];
		cell.imageView.image = [UIImage imageNamed:@"cross"];
		return cell;
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	sVideoURL = [[VideoListArray objectAtIndex:indexPath.row] objectForKey:@"videourl"];
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	[VideoTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

#pragma mark - WebService Methods
-(void)ServerRequestGallery {
	APINo = 1;
	serverclass.delegateSCD = self;
	NSString *payLoadString = [NSString stringWithFormat:@"app_id=%@&offset=0&limit=9",kAppId];
	NSString *apiName		= [NSString stringWithFormat:@"imagegallerylistapi.php"];
	[serverclass PostServerRequest:apiName Parameters:payLoadString];
	[self processStart];
}

-(void)ServerRequestVideo {
	APINo = 2;
	serverclass.delegateSCD = self;
	NSString *payLoadString = [NSString stringWithFormat:@"app_id=%@&offset=0&limit=9",kAppId];
	NSString *apiName		= [NSString stringWithFormat:@"videoapi.php"];
	[serverclass PostServerRequest:apiName Parameters:payLoadString];
	[self processStart];
}

-(void)ServerCallBackResponse:(NSData *)serverResponse {
	[self processEnd];
	NSError *error = nil;
	NSData *objectData  = [[[NSString alloc] initWithData:serverResponse encoding:NSUTF8StringEncoding] dataUsingEncoding:NSUTF8StringEncoding];
	NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:objectData
															 options:NSJSONReadingAllowFragments
															   error:&error];
	if (!jsonDict) {
		NSLog(@"Error parsing JSON: %@", error);
	}else {
//		NSLog(@"JSON: %@", jsonDict);
		if (APINo == 1) {
			[GalleryListArray removeAllObjects];
			if ([[jsonDict objectForKey:@"status"] isEqualToString:@"success"]) {
				GalleryListArray = [jsonDict objectForKey:@"albumlist"];
				[GalleryCollectionView reloadData];
			}
		}else if (APINo == 2) {
			[VideoListArray removeAllObjects];
			if ([[jsonDict objectForKey:@"status"] isEqualToString:@"success"]) {
				VideoListArray = [jsonDict objectForKey:@"videodata"];
				[VideoTableView reloadData];
			}
		}
	}
}

#pragma mark - UIActivityIndicator Methods
- (void)processStart {
	[indicator setHidden:NO];
	[indicator startAnimating];
}

- (void)processEnd {
	[indicator setHidden:YES];
	[indicator stopAnimating];
}
@end
