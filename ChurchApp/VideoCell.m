//
//  VideoCell.m
//  ChurchApp
//
//  Created by Grigo Mathews on 25/02/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import "VideoCell.h"

@implementation VideoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
