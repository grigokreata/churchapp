//
//  GalleryCell.h
//  ChurchApp
//
//  Created by Grigo Mathews on 09/02/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GalleryCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *CardView;
@property (weak, nonatomic) IBOutlet UIImageView *gImageView;
@property (weak, nonatomic) IBOutlet UILabel *gTitle;

@end
