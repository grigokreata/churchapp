//
//  CAPSPageMenuViewController.h
//  ChurchApp
//
//  Created by Grigo Mathews on 21/02/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CAPSPageMenuViewController : UIViewController

@property (strong, nonatomic) NSArray  *CAPSPrayerArray;

@end
