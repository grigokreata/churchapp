//
//  CAPSPageMenuViewController.m
//  ChurchApp
//
//  Created by Grigo Mathews on 21/02/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import "CAPSPageMenuViewController.h"
#import "PrayerBookViewController.h"
#import "CAPSPageMenu.h"
#import "UIColor+GlobalColor.h"

@interface CAPSPageMenuViewController ()
{
	NSMutableArray *controllerArray;
}

@property (weak, nonatomic) IBOutlet UIImageView *headerView;
@property (nonatomic) CAPSPageMenu *pageMenu;

- (IBAction)BackBtnAction:(id)sender;
@end

@implementation CAPSPageMenuViewController
@synthesize CAPSPrayerArray;
@synthesize headerView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	controllerArray = [[NSMutableArray alloc] init];
	headerView.backgroundColor = [UIColor headerBgColor];
	[self SettingMenu:[CAPSPrayerArray count]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)BackBtnAction:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Setting Menu
- (void)SettingMenu:(long) arrayCount {
	UIStoryboard *mystoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
	for (int i=0; i<arrayCount; i++) {
		PrayerBookViewController *controller1 = [mystoryboard instantiateViewControllerWithIdentifier:@"PrayerBookViewController"];
		controller1.title = CAPSPrayerArray[i];
		controller1.passingNo = i+1;
		[controllerArray addObject:controller1];
	}
	float MenuWidth = self.view.bounds.size.width/2;
	NSDictionary *parameters = @{
								 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor headerBgColor],//menu bg
								 CAPSPageMenuOptionViewBackgroundColor: [UIColor whiteColor],//TableBg
								 CAPSPageMenuOptionSelectionIndicatorColor: [UIColor whiteColor], //selected tab
								 CAPSPageMenuOptionUnselectedMenuItemLabelColor: [UIColor whiteColor],
								 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor whiteColor],
								 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"Signika-Regular" size:15.0],
								 CAPSPageMenuOptionMenuHeight: @(40.0),
								 CAPSPageMenuOptionMenuMargin: @(0.0),
								 CAPSPageMenuOptionMenuItemWidth: @(MenuWidth),
								 //								 CAPSPageMenuOptionMenuItemWidthBasedOnTitleTextWidth:@(YES),
								 CAPSPageMenuOptionCenterMenuItems: @(YES)
								 };
	
	_pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 64.0, self.view.frame.size.width, self.view.frame.size.height) options:parameters];
	[self.view addSubview:_pageMenu.view];
}
@end
