//
//  PrayerCell2.h
//  ChurchApp
//
//  Created by Grigo Mathews on 23/02/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrayerCell2 : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *daylbl;
@property (weak, nonatomic) IBOutlet UILabel *timelbl;
@end
