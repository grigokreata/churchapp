package com.flypaper.android.churchapp.util;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.flypaper.android.churchapp.listener.VolleyResponseListener;
import com.flypaper.android.churchapp.volley.VolleyNetworkConnect;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by nfonics on 11-Dec-15.
 */
public class AppManager {

    private Request.Priority priority = Request.Priority.HIGH;

    private String TAG = "AppManager";
    Common commonUtil = new Common();

    public void GetNews(final String reqType, final int category, final int offset, final VolleyResponseListener responseListener) {
        StringRequest GetNewsRequest = new StringRequest(Request.Method.POST, CommonUtil.BASE_URL + "report.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                responseListener.onResponse(reqType, s);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("app_id", CommonUtil.APP_ID);
                params.put("category", String.valueOf(category));
                params.put("limit", "9");
                params.put("offset", String.valueOf(offset));
                return params;
            }

            @Override
            public Priority getPriority() {
                return priority;
            }
        };
        VolleyNetworkConnect.getInstance().addToRequestQueue(GetNewsRequest, "GetNewsRequest");
    }

    public void FetchMedia(final String reqType, final String urlAppend, final int offset, final VolleyResponseListener responseListener) {
        StringRequest GetNewsRequest = new StringRequest(Request.Method.POST, CommonUtil.BASE_URL + urlAppend, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                responseListener.onResponse(reqType, s);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("app_id", CommonUtil.APP_ID);
                params.put("limit", "9");
                params.put("offset", String.valueOf(offset));
                return params;
            }

            @Override
            public Priority getPriority() {
                return priority;
            }
        };
        VolleyNetworkConnect.getInstance().addToRequestQueue(GetNewsRequest, "GetNewsRequest");
    }

    public void GetAlbumImages(final String reqType, final String albumId, final int offset, final VolleyResponseListener responseListener) {
        StringRequest GetNewsRequest = new StringRequest(Request.Method.POST, CommonUtil.BASE_URL + "albumimages.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                responseListener.onResponse(reqType, s);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("albumid", albumId);
                params.put("limit", "9");
                params.put("offset", String.valueOf(offset));
                return params;
            }

            @Override
            public Priority getPriority() {
                return priority;
            }
        };
        VolleyNetworkConnect.getInstance().addToRequestQueue(GetNewsRequest, "GetNewsRequest");
    }

    public void GetEvents(final String reqType, final String month, final String year, final VolleyResponseListener responseListener) {
        StringRequest GetEventsRequest = new StringRequest(Request.Method.POST, CommonUtil.BASE_URL + "eventapi.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                responseListener.onResponse(reqType, s);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("app_id", CommonUtil.APP_ID);
                params.put("month", month);
                params.put("year", year);
                return params;
            }

            @Override
            public Priority getPriority() {
                return priority;
            }
        };
        VolleyNetworkConnect.getInstance().addToRequestQueue(GetEventsRequest, "GetEventsRequest");
    }

    public void UpdateGCMId(final String reqType, final String gcmId, final VolleyResponseListener responseListener) {
        StringRequest GetMsgRequest = new StringRequest(Request.Method.POST, CommonUtil.BASE_URL + "device.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                responseListener.onResponse(reqType, s);
                Log.e("res", "" + s);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("res", "erre");
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("gcm", gcmId);
                params.put("device", "andriod / mobile");
                params.put("app_id", CommonUtil.APP_ID);
                return params;
            }

            @Override
            public Priority getPriority() {
                return priority;
            }
        };
        VolleyNetworkConnect.getInstance().addToRequestQueue(GetMsgRequest, "GetMsgRequest");
    }


}
