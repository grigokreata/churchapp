//
//  NewsViewController.m
//  ChurchApp
//
//  Created by Grigo Mathews on 31/01/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import "NewsViewController.h"
#import "GlobalIdentifiers.h"
#import "ServerClass.h"
#import "NewsCell.h"
#import "UIImageView+WebCache.h"
#import "NewsDetailViewController.h"


@interface NewsViewController ()<ServerClassDelegate, UITableViewDelegate, UITableViewDataSource>
{
	ServerClass *serverclass;
	NSArray *NewsArray;
}
@property (weak, nonatomic) IBOutlet UITableView *NewsTableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (IBAction)BackBtnAction:(id)sender;

@end

@implementation NewsViewController
@synthesize NewsTableView;
@synthesize indicator;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	serverclass = [[ServerClass alloc] init];
	[self ServerRequestNews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)BackBtnAction:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [NewsArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 109;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *cellIdentifier = @"NewsCell";
	
	NewsCell *hCell = (NewsCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	if(hCell == nil) {
		NSArray *customObjects = [[NSBundle mainBundle]loadNibNamed:@"NewsCell" owner:self options:nil];
		for(id objects in customObjects){
			if([objects isKindOfClass:[UITableViewCell class]]) {
				hCell = (NewsCell *)objects;
			}
		}
	}
	hCell.NewsLbl1.text = [NSString stringWithFormat:@"%@",[[NewsArray objectAtIndex:indexPath.row] objectForKey:@"message"]];
	NSURL *fimage = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[[NewsArray objectAtIndex:indexPath.row] objectForKey:@"image"]]];
	[hCell.NewsImage sd_setImageWithURL:fimage placeholderImage:[UIImage imageNamed:@"cross"]];
	
	return hCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UIStoryboard *mystoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
	NewsDetailViewController *NewsDetailVC = [mystoryboard instantiateViewControllerWithIdentifier:@"NewsDetailViewController"];
	NewsDetailVC.passingDic = [NewsArray objectAtIndex:indexPath.row];
	[self.navigationController pushViewController:NewsDetailVC animated:YES];
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - WebService Methods
-(void)ServerRequestNews {
	serverclass.delegateSCD = self;
	NSString *payLoadString = [NSString stringWithFormat:@"app_id=%@&category=1&offset=0&limit=9",kAppId];
	NSString *apiName		= [NSString stringWithFormat:@"report.php"];
	[serverclass PostServerRequest:apiName Parameters:payLoadString];
	[self processStart];
}

-(void)ServerCallBackResponse:(NSData *)serverResponse {
	[self processEnd];
	NSError *error = nil;
	NSData *objectData  = [[[NSString alloc] initWithData:serverResponse encoding:NSUTF8StringEncoding] dataUsingEncoding:NSUTF8StringEncoding];
	NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:objectData
															 options:NSJSONReadingAllowFragments
															   error:&error];
	if (!jsonDict) {
		NSLog(@"Error parsing JSON: %@", error);
	}else {
		NSLog(@"JSON: %@", jsonDict);
		if ([[jsonDict objectForKey:@"status"] isEqualToString:@"success"]) {
			NewsArray = [jsonDict objectForKey:@"newsdata"];
		}
//		NSLog(@"NewsArray: %@", NewsArray);
		[NewsTableView reloadData];
	}
}

#pragma mark - UIActivityIndicator Methods
- (void)processStart {
	[indicator setHidden:NO];
	[indicator startAnimating];
}

- (void)processEnd {
	[indicator setHidden:YES];
	[indicator stopAnimating];
}

@end
