//
//  MenuCell.h
//  ChurchApp
//
//  Created by Grigo Mathews on 30/01/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *mCellImage;
@property (weak, nonatomic) IBOutlet UILabel *mCelllbl;

@end
