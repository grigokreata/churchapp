//
//  HBChapterListViewController.m
//  ChurchApp
//
//  Created by Grigo Mathews on 13/02/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import "HBChapterListViewController.h"
#import "HBCListCell.h"
#import "HBDetailViewController.h"

static NSString *ListCellId = @"HBCListCell";

@interface HBChapterListViewController ()<UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *pageTitle;
@property (weak, nonatomic) IBOutlet UICollectionView *hbcCollectionView;

- (IBAction)BackBtnAction:(id)sender;

@end

@implementation HBChapterListViewController
@synthesize PassingHeaderText;
@synthesize BibleArray;
@synthesize pageTitle;
@synthesize hbcCollectionView;

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	pageTitle.text = PassingHeaderText;
	//	NSLog(@"PassingHeaderText: %@",pageTitle.text);
	NSLog(@"BibleArray count: %lu",(unsigned long)[BibleArray count]);
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)BackBtnAction:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
	return [BibleArray count];
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath  {
	
	HBCListCell *lCell	= (HBCListCell *)[collectionView dequeueReusableCellWithReuseIdentifier:ListCellId forIndexPath:indexPath];
	lCell.hbclbl1.text = [NSString stringWithFormat:@"%ld", (long)indexPath.row];
	return lCell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
	return CGSizeMake(60, 60);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
	return UIEdgeInsetsMake(10, 10, 10, 10);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath  {
	NSLog(@"didSelectItemAtIndexPath: %@", [BibleArray objectAtIndex:indexPath.row]);
	UIStoryboard *mystoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
	HBDetailViewController *HBDetailVC = [mystoryboard instantiateViewControllerWithIdentifier:@"HBDetailViewController"];
	HBDetailVC.passingTitle = PassingHeaderText;
	HBDetailVC.passingText	= [BibleArray objectAtIndex:indexPath.row];
	[self.navigationController pushViewController:HBDetailVC animated:YES];
}

@end
