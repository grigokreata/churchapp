//
//  OfferingsViewController.m
//  ChurchApp
//
//  Created by Grigo Mathews on 31/01/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import "OfferingsViewController.h"
#import "OTextFieldCell.h"
#import "OPayCell.h"
#import "SelectViewController.h"
#import "GlobalIdentifiers.h"

@protocol UserChatDelegate <NSObject>

@optional

- (void)GetbackCategory:(NSArray *)arr;

@end


@interface OfferingsViewController ()<UITableViewDelegate, UITableViewDataSource, OfferingsDelegate, UITextFieldDelegate>
{
	NSString *ButtonText;
	int TotalRate;
	NSString *oName;
	NSString *oPhone;
	NSString *oEmail;
}

@property (weak, nonatomic) IBOutlet UITableView *OfferTableView;

- (IBAction)BackBtnAction:(id)sender;

@end

@implementation OfferingsViewController
@synthesize OfferTableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	ButtonText = @"Select category";
	TotalRate  = 0;
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:YES];
	[OfferTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)BackBtnAction:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 2;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (section == 0) {
		return 4;
	}else {
		return 1;
	}
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.section == 0) {
		if (indexPath.row == 0) {
			return 71;
		}else {
			return 51;
		}
	}else {
		return 261;
	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *cellId1 = @"OTextFieldCell";
	static NSString *cellId2 = @"OPayCell";

	if (indexPath.section == 0) {
		OTextFieldCell *oCell = (OTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:cellId1];
		if(oCell == nil) {
			NSArray *customObjects = [[NSBundle mainBundle]loadNibNamed:@"OTextFieldCell" owner:self options:nil];
			for(id objects in customObjects){
				if([objects isKindOfClass:[UITableViewCell class]]) {
					oCell = (OTextFieldCell *)objects;
				}
			}
		}
		oCell.nametxt.delegate = self;
		if (indexPath.row == 0) {
			oCell.nametxt.placeholder = @"Enter your name";
			oCell.nametxt.keyboardType = UIKeyboardTypeDefault;
			[[NSNotificationCenter defaultCenter] addObserver:self
													 selector:@selector(NameTextDidChange:)
														 name:UITextFieldTextDidChangeNotification
													   object:oCell.nametxt];
		}else if (indexPath.row == 1) {
			oCell.nametxt.placeholder = @"Enter phone number";
			oCell.nametxt.keyboardType = UIKeyboardTypeNumberPad;
			[[NSNotificationCenter defaultCenter] addObserver:self
													 selector:@selector(PhoneTextDidChange:)
														 name:UITextFieldTextDidChangeNotification
													   object:oCell.nametxt];
		}else if (indexPath.row == 2) {
			oCell.nametxt.placeholder = @"Enter your email";
			oCell.nametxt.keyboardType = UIKeyboardTypeEmailAddress;
			[[NSNotificationCenter defaultCenter] addObserver:self
													 selector:@selector(EmailTextDidChange:)
														 name:UITextFieldTextDidChangeNotification
													   object:oCell.nametxt];
		}else {
			oCell.nametxt.tintColor   = [UIColor blackColor];
			oCell.nametxt.userInteractionEnabled = NO;
			oCell.nametxt.placeholder = @"";
			[oCell.nametxt setText:ButtonText];
		}
		return oCell;
	}else {
		OPayCell *oCell = (OPayCell *)[tableView dequeueReusableCellWithIdentifier:cellId2];
		if(oCell == nil) {
			NSArray *customObjects = [[NSBundle mainBundle]loadNibNamed:@"OPayCell" owner:self options:nil];
			for(id objects in customObjects){
				if([objects isKindOfClass:[UITableViewCell class]]) {
					oCell = (OPayCell *)objects;
				}
			}
		}
		oCell.PayNowBtn.layer.cornerRadius = 20.0f;
		oCell.Ratelbl.text = [NSString stringWithFormat:@"%d",TotalRate];
		[oCell.PayNowBtn addTarget:self action:@selector(PayNowBtnAction:) forControlEvents:UIControlEventTouchUpInside];
		return oCell;
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.section == 0) {
		if (indexPath.row == 3) {
			UIStoryboard *mystoryboard		= [UIStoryboard storyboardWithName:@"Main" bundle:nil];
			SelectViewController *SelectVC	= [mystoryboard instantiateViewControllerWithIdentifier:@"SelectViewController"];
			SelectVC.delegateOD				= self;
			[self.navigationController pushViewController:SelectVC animated:YES];
		}
	}
}

#pragma mark - Custom Delegate Methods
- (void)getSelectedArray:(NSMutableArray *)ratearray MalArray:(NSMutableArray *)malarray {
	
	int TRate = 0;
	NSString *btnStr = @"";
	for (NSString *rate in ratearray) {
		TRate += [rate intValue];
	}
	for (NSString *mal in malarray) {
		if ([btnStr isEqualToString:@""]) {
			btnStr = mal;
		}else {
			btnStr = [NSString stringWithFormat:@"%@, %@",btnStr, mal];
		}
	}
	TotalRate	= TRate;
	ButtonText	= btnStr;
	NSLog(@"Rate: %d\nmalarray: %@", TRate, btnStr);
}


#pragma mark - UITableViewButtonAction
- (IBAction)PayNowBtnAction:(id)sender {
	NSLog(@"%@",oName);
	if (oName == nil) {
		if ([UIAlertController class]) {
			UIAlertController *alertController = [UIAlertController alertControllerWithTitle:kAlertTitle message:kNoNameMessage preferredStyle:UIAlertControllerStyleAlert];
			UIAlertAction* ok = [UIAlertAction actionWithTitle:kOk style:UIAlertActionStyleDefault handler:nil];
			[alertController addAction:ok];
			[self presentViewController:alertController animated:YES completion:nil];
		}
	}else {
		if (oPhone == nil) {
			if ([UIAlertController class]) {
				UIAlertController *alertController = [UIAlertController alertControllerWithTitle:kAlertTitle message:kNoPhoneMessage preferredStyle:UIAlertControllerStyleAlert];
				UIAlertAction* ok = [UIAlertAction actionWithTitle:kOk style:UIAlertActionStyleDefault handler:nil];
				[alertController addAction:ok];
				[self presentViewController:alertController animated:YES completion:nil];
			}
		}else {
			if (oEmail == nil) {
				if ([UIAlertController class]) {
					UIAlertController *alertController = [UIAlertController alertControllerWithTitle:kAlertTitle message:kNoEmailMessage preferredStyle:UIAlertControllerStyleAlert];
					UIAlertAction* ok = [UIAlertAction actionWithTitle:kOk style:UIAlertActionStyleDefault handler:nil];
					[alertController addAction:ok];
					[self presentViewController:alertController animated:YES completion:nil];
				}
			}else {
				NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
				NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
				if(!([emailTest evaluateWithObject:oEmail] == YES) ) {
					if ([UIAlertController class]) {
						UIAlertController *alertController = [UIAlertController alertControllerWithTitle:kAlertTitle message:kValidEmailMessage preferredStyle:UIAlertControllerStyleAlert];
						UIAlertAction* ok = [UIAlertAction actionWithTitle:kOk style:UIAlertActionStyleDefault handler:nil];
						[alertController addAction:ok];
						[self presentViewController:alertController animated:YES completion:nil];
					}
				}else {
					if (TotalRate == 0) {
						if ([UIAlertController class]) {
							UIAlertController *alertController = [UIAlertController alertControllerWithTitle:kAlertTitle message:kNoEmailMessage preferredStyle:UIAlertControllerStyleAlert];
							UIAlertAction* ok = [UIAlertAction actionWithTitle:kOk style:UIAlertActionStyleDefault handler:nil];
							[alertController addAction:ok];
							[self presentViewController:alertController animated:YES completion:nil];
						}
					}else {
						
					}
				}
			}
		}
	}
}

#pragma mark -   UITextFieldNotificationMethods
- (void)NameTextDidChange:(NSNotification *)notification {
	UITextField *cpText = (UITextField *)[notification object];
	oName = cpText.text;
}

- (void)PhoneTextDidChange:(NSNotification *)notification {
	UITextField *cpText = (UITextField *)[notification object];
	oPhone = cpText.text;
}
- (void)EmailTextDidChange:(NSNotification *)notification {
	UITextField *cpText = (UITextField *)[notification object];
	oEmail = cpText.text;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	[textField resignFirstResponder];
	return YES;
}

@end
