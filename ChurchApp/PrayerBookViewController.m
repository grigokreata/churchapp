//
//  PrayerBookViewController.m
//  ChurchApp
//
//  Created by Grigo Mathews on 31/01/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import "PrayerBookViewController.h"
#import "PrayerCell1.h"
#import "PrayerCell2.h"
#import "PrayerCell3.h"
#import "PrayerCell4.h"
#import "GlobalIdentifiers.h"
#import "ServerClass.h"

@interface PrayerBookViewController ()<UITableViewDelegate, UITableViewDataSource, ServerClassDelegate>
{
	NSArray *pTimeArray;
	NSString *TodaysPrayer;
	ServerClass *serverclass;
}
@property (weak, nonatomic) IBOutlet UITableView *PrayerTableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (IBAction)BackBtnAction:(id)sender;

@end

@implementation PrayerBookViewController
@synthesize passingNo;
@synthesize PrayerTableView;
@synthesize indicator;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	[self processEnd];
	if (passingNo == 1) {
		pTimeArray = [NSArray arrayWithObjects:
					  @{@"day": @"sunday",		@"time": @"5.30, 7.00, 8.45, 11.00am, 4.30pm"},
					  @{@"day": @"monday",		@"time": @"5:30, 6:30, 7:30 am"},
					  @{@"day": @"tuesday",		@"time": @"5:30, 6:30, 7:30 am"},
					  @{@"day": @"wednesday",	@"time": @"5:30, 6:30, 7:30 am"},
					  @{@"day": @"thursday",	@"time": @"5:30, 6:30, 7:30 am"},
					  @{@"day": @"friday",		@"time": @"5:30, 6:30,(Novena- St.Sebastian's)  7:30 am, 4:30 pm. Holy Mass with Novena of St.Sebastian"},
					  @{@"day": @"saturday",	@"time": @"5:30, 6:30,(Novena- Muthiyamma) 7:30, 8:30 am,Holy Mass with novena 5.00pm : Holy Mass,Novena and Candle Rosary Procession"},
					  nil];
		
	}else {
		TodaysPrayer = @"";
		serverclass = [[ServerClass alloc] init];
		[self ServerRequestPrayerBook];
	}
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)BackBtnAction:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	if (passingNo == 1) {
		return 3;
	}else {
		return 1;
	}
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (passingNo == 1) {
		if (section == 1) {
			return [pTimeArray count];
		}else {
			return 1;
		}
	}else {
		return 1;
	}
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *cellIdentifier = @"cellIdentifier";
//	static NSString *cellId1 = @"PrayerCell1";
//	static NSString *cellId2 = @"PrayerCell2";
	
	if (passingNo == 1) {
		if (indexPath.section == 0) {
			return 450;
		}else if (indexPath.section == 1) {
			if (indexPath.row == 5) {
				return 60;
			}else if (indexPath.row == 6) {
				return 70;
			}else  {
				return 50;
			}
			
		}else if (indexPath.section == 2) {
			return 104;
		}else {
			return 0;
		}
	}else {
		
		UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
		if(cell == nil) {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
			cell.textLabel.font				= [UIFont fontWithName:@"Signika-Regular" size:15.0];
			cell.selectionStyle				= UITableViewCellSelectionStyleNone;
			cell.textLabel.lineBreakMode	= NSLineBreakByWordWrapping;
			cell.textLabel.numberOfLines	= 0;
		}
		
		CGSize maximumLabelSize = CGSizeMake(296, 480);
		CGFloat width = maximumLabelSize.width;
		UIFont *font = cell.textLabel.font;
		NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:TodaysPrayer attributes:@{NSFontAttributeName: font}];
		CGRect rect = [attributedText boundingRectWithSize:(CGSize){width, CGFLOAT_MAX} options:NSStringDrawingUsesLineFragmentOrigin  context:nil];
		CGSize size = rect.size;
		
		//adjust the label the the new height.
		CGRect newFrame = cell.textLabel.frame;
		newFrame.size.height = size.height;
		cell.textLabel.frame = newFrame;
		CGFloat height = newFrame.size.height+50;
		return height;
	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *cellIdentifier = @"cellIdentifier";
	static NSString *cellId1 = @"PrayerCell1";
	static NSString *cellId2 = @"PrayerCell2";
	if (passingNo == 1) {
		if (indexPath.section == 0) {
			PrayerCell1 *p1Cell = (PrayerCell1 *)[tableView dequeueReusableCellWithIdentifier:cellId1];
			if(p1Cell == nil) {
				NSArray *customObjects = [[NSBundle mainBundle]loadNibNamed:@"PrayerCell1" owner:self options:nil];
				for(id objects in customObjects){
					if([objects isKindOfClass:[UITableViewCell class]]) {
						p1Cell = (PrayerCell1 *)objects;
					}
				}
			}
			p1Cell.heading1.text = @"Prayer to Muthiyamma";
			p1Cell.heading2.text = @"Workship Timing";
			p1Cell.heading3.text = @"HOLY MASS TIME TABLE";
			p1Cell.body1.text	 = @"പരിശുദ്ധ അമ്മേ, കുറവിലങ്ങാട് മുത്തിയമ്മേ,മനുഷ്യകുലം മുഴുവന്റെയും മദ്ധ്യസ്ഥയും സംരക്ഷകയുമായ അങ്ങയെ ഞങ്ങള്‍ വണങ്ങുകയും, അങ്ങേയ്ക്കു ഞങ്ങള്‍ കൃതജ്ഞത അര്‍പ്പിക്കുകയും ചെയ്യുന്നു. ഞങ്ങളുടെ മാതാവും സംരക്ഷകയുമായി ഞങ്ങള്‍ അങ്ങയെ സ്വീകരിക്കുന്നു. കരുണാസമ്പന്നയായ മുത്തിയമ്മേ, ഏറ്റവും വിശ്വാസേത്താടെ ഞങ്ങള്‍ അങ്ങയില്‍ ശരണപ്പെടുന്നു. അങ്ങയുടെ ശക്തിയേറിയ തിരുനാമം ഞങ്ങള്‍ വിളിച്ചപേക്ഷിക്കുന്നു. അങ്ങയുടെ മദ്ധ്യസ്ഥശക്തിയാല്‍ ആത്മീയവും ശാരീരികവുമായ എല്ലാ ആപത്തുകളില്‍നിന്നും പ്രത്യേകിച്ച് പൈശാചകഉപദ്രവങ്ങളില്‍നിന്നും പ്രലോഭനങ്ങളില്‍നിന്നും തകര്‍ച്ചകളില്‍നിന്നും രോഗപീഢകളില്‍നിന്നും ഞങ്ങളെ സംരക്ഷിക്കണമേ. അങ്ങില്‍ ദൃഢമായി വിശ്വസിക്കുന്ന ഞങങ്ങളുടെ എല്ലാ പ്രാര്‍ത്ഥനകളും, പ്രത്യേകിച്ച് (നിയോഗം) അങ്ങേ തിരുക്കുമാരനായ ഈശോയ്ക്ക് സമര്‍പ്പിച്ച് ഞങ്ങള്‍ക്ക് സാധിച്ചു തരണമേ. പാപരഹിതമായ ജീവിതംവഴി അങ്ങയോടും അങ്ങേ തിരുക്കുമാരനോടും എന്നും വിശ്വസ്തരായിരുന്നുകൊള്ളാമെന്ന് ഞങ്ങള്‍ വാഗ്ദാനം ചെയ്യുകയും ചെയ്യുന്നു. ആമ്മേന്.";
			[p1Cell.btn1 setTitle:@"SyroMalabar Yaama Praarthanakal (click)" forState:UIControlStateNormal];
			[p1Cell.btn1 addTarget:self action:@selector(BookClicked:) forControlEvents:UIControlEventTouchUpInside];
			return p1Cell;
		}else if (indexPath.section == 1) {
			PrayerCell2 *p2Cell = (PrayerCell2 *)[tableView dequeueReusableCellWithIdentifier:cellId2];
			if(p2Cell == nil) {
				NSArray *customObjects = [[NSBundle mainBundle]loadNibNamed:@"PrayerCell2" owner:self options:nil];
				for(id objects in customObjects){
					if([objects isKindOfClass:[UITableViewCell class]]) {
						p2Cell = (PrayerCell2 *)objects;
					}
				}
			}
			p2Cell.daylbl.text  = [[[pTimeArray objectAtIndex:indexPath.row] objectForKey:@"day"] uppercaseString];
			p2Cell.timelbl.text = [[pTimeArray objectAtIndex:indexPath.row] objectForKey:@"time"];
			return p2Cell;
		}else if (indexPath.section == 2) {
			UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
			if(cell == nil) {
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
				cell.textLabel.font					= [UIFont fontWithName:@"Signika-Semibold" size:17.0];
				cell.detailTextLabel.font			= [UIFont fontWithName:@"Signika-Semibold" size:15.0];
				cell.detailTextLabel.textColor		= [UIColor darkGrayColor];
				cell.detailTextLabel.lineBreakMode	= NSLineBreakByWordWrapping;
				cell.selectionStyle					= UITableViewCellSelectionStyleNone;
				cell.detailTextLabel.numberOfLines	= 0;
			}
			cell.textLabel.text = @"FIRST FRIDAY SPECIAL MASS";
			cell.detailTextLabel.text = @"4.30am, 5.30am, 6.30am, 7.30am, 8.30am, 9.30am,10.30am (Suriyani Qurbana), 12.00, 2.45pm. 4.00pm,5.30pm, 8.00pm";
			return cell;
		}else {
			UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
			if(cell == nil) {
				cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
			}
			return cell;
		}
	}else {
		UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
		if(cell == nil) {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
			cell.textLabel.font				= [UIFont fontWithName:@"Signika-Regular" size:15.0];
			cell.selectionStyle				= UITableViewCellSelectionStyleNone;
			cell.textLabel.lineBreakMode	= NSLineBreakByWordWrapping;
			cell.textLabel.numberOfLines	= 0;
		}
		cell.textLabel.text = TodaysPrayer;
		return cell;
	}
}

#pragma mark - UITableView ButtonAction
- (IBAction)BookClicked:(id)sender {
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:kGooglePlayStoreURL]];
}

#pragma mark - WebService Methods
-(void)ServerRequestPrayerBook {
	serverclass.delegateSCD = self;
	NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd"];
	NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
	NSString *payLoadString = [NSString stringWithFormat:@"app_id=%@&date=%@", kAppId, [dateFormatter stringFromDate:[NSDate date]]];
	NSString *apiName		= [NSString stringWithFormat:@"prayapi.php"];
	[serverclass PostServerRequest:apiName Parameters:payLoadString];
	[self processStart];
}

-(void)ServerCallBackResponse:(NSData *)serverResponse {
	[self processEnd];
	NSError *error = nil;
	NSData *objectData  = [[[NSString alloc] initWithData:serverResponse encoding:NSUTF8StringEncoding] dataUsingEncoding:NSUTF8StringEncoding];
	NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:objectData
															 options:NSJSONReadingAllowFragments
															   error:&error];
	if (!jsonDict) {
		NSLog(@"Error parsing JSON: %@", error);
	}else {
		NSLog(@"JSON: %@", jsonDict);
		if ([[jsonDict objectForKey:@"status"] isEqualToString:@"success"]) {
			NSLog(@"%@",[[[jsonDict objectForKey:@"praydata"] objectAtIndex:0] objectForKey:@"id"]);
			TodaysPrayer = [[[jsonDict objectForKey:@"praydata"] objectAtIndex:0] objectForKey:@"pray"];
		}
		[PrayerTableView reloadData];
	}
}

#pragma mark - UIActivityIndicator Methods
- (void)processStart {
	[indicator setHidden:NO];
	[indicator startAnimating];
}

- (void)processEnd {
	[indicator setHidden:YES];
	[indicator stopAnimating];
}

@end
