//
//  ServerClass.h
//  ChurchApp
//
//  Created by Grigo Mathews on 30/01/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ServerClassDelegate <NSObject>

-(void)ServerCallBackResponse:(NSData *)serverResponse;

@end

@interface ServerClass : NSObject<NSURLSessionDelegate>
{
	NSMutableData *webData;
}

@property (strong, nonatomic) id<ServerClassDelegate> delegateSCD;

- (BOOL)connected;
- (void)GetServerRequest:(NSString *)apiName;
- (void)PostServerRequest:(NSString *)apiName Parameters:(NSString *)parameters;
- (NSString *)encodeString:(NSString *)string;
- (void)AddUserDefaults:(id)aobject Key:(NSString *)akey;
- (NSString *)FetchUserDefaults:(NSString *)fkey;

@end
