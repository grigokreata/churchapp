//
//  HistoryViewController.m
//  ChurchApp
//
//  Created by Grigo Mathews on 31/01/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import "HistoryViewController.h"
#import "HImageCell.h"
#import "HTextCell.h"

@interface HistoryViewController ()<UITableViewDelegate, UITableViewDataSource>
{
	NSString *historyText;
	NSMutableAttributedString *attrString;
	long LStatus; // LStatus=0 Malayalam, LStatus=1 English
	long NoOfRows;
	NSString *body1, *heading2, *body2, *heading3, *body3, *heading4, *body4, *heading5, *body5, *heading6, *body6, *heading7, *body7, *heading81, *heading82, *body8, *heading9, *body9, *heading10, *body10, *heading11, *body11, *heading12, *body12;
}
@property (weak, nonatomic) IBOutlet UITextView *HTextView;
@property (weak, nonatomic) IBOutlet UILabel *HLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *HScrollView;
@property (weak, nonatomic) IBOutlet UITableView *HTableView;

- (IBAction)BackBtnAction:(id)sender;
- (IBAction)ToggleBtnAction:(id)sender;

@end

@implementation HistoryViewController
@synthesize HTextView;
@synthesize HLabel;
@synthesize HScrollView;
@synthesize HTableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	LStatus = 0;NoOfRows=12;
	HTableView.delegate		= self;
	HTableView.dataSource	= self;
	[self ChangeToMalayalam];
//	HScrollView.contentSize = CGSizeMake(self.view.bounds.size.width, 800);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - IBAction Methods
- (IBAction)BackBtnAction:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)ToggleBtnAction:(id)sender {
	if (LStatus == 1) {
		[self ChangeToMalayalam];
//		NoOfRows = 12;
	}else {
		[self ChangeToEnglish];
		NoOfRows = 1;
	}
	[HTableView reloadData];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	switch (section) {
		case 0:
			return 1;
			break;
		case 1:
			return NoOfRows;
//			return 1;
			break;
		default:
			return 0;
			break;
	}
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.section == 0) {
		return 201;
	}else if (indexPath.section == 1) {
		HTextCell *hCell = (HTextCell *)[tableView dequeueReusableCellWithIdentifier:@"HTextCell"];
		if(hCell == nil) {
			NSArray *customObjects = [[NSBundle mainBundle]loadNibNamed:@"HTextCell" owner:self options:nil];
			for(id objects in customObjects){
				if([objects isKindOfClass:[UITableViewCell class]]) {
					hCell = (HTextCell *)objects;
				}
			}
		}
		return 200;
	}else {
		return 0;
	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *cellIdentifier = @"cellIdentifier";
	static NSString *cellId1 = @"HImageCell";
	static NSString *cellId2 = @"HTextCell";
	
	if (indexPath.section == 0) {
		HImageCell *hCell = (HImageCell *)[tableView dequeueReusableCellWithIdentifier:cellId1];
		if(hCell == nil) {
			NSArray *customObjects = [[NSBundle mainBundle]loadNibNamed:@"HImageCell" owner:self options:nil];
			for(id objects in customObjects){
				if([objects isKindOfClass:[UITableViewCell class]]) {
					hCell = (HImageCell *)objects;
				}
			}
		}
		return hCell;
	}else if (indexPath.section == 1) {
		HTextCell *hCell = (HTextCell *)[tableView dequeueReusableCellWithIdentifier:cellId2];
		if(hCell == nil) {
			NSArray *customObjects = [[NSBundle mainBundle]loadNibNamed:@"HTextCell" owner:self options:nil];
			for(id objects in customObjects){
				if([objects isKindOfClass:[UITableViewCell class]]) {
					hCell = (HTextCell *)objects;
				}
			}
		}
		if (LStatus == 1) {
			hCell.Hlbl1.text = historyText;
		}else {
			if (indexPath.row == 0) {
				hCell.Hlbl1.text = body1;
			}else if (indexPath.row == 1) {
				hCell.Hlbl1.text = [NSString stringWithFormat:@"\n\n\n%@\n\n%@",heading2, body2];
			}else if (indexPath.row == 2) {
				hCell.Hlbl1.text = historyText;
			}else if (indexPath.row == 3) {
				hCell.Hlbl1.text = historyText;
			}else if (indexPath.row == 4) {
				hCell.Hlbl1.text = historyText;
			}else if (indexPath.row == 5) {
				hCell.Hlbl1.text = historyText;
			}else if (indexPath.row == 6) {
				hCell.Hlbl1.text = historyText;
			}else if (indexPath.row == 7) {
				hCell.Hlbl1.text = historyText;
			}else if (indexPath.row == 8) {
				hCell.Hlbl1.text = historyText;
			}else if (indexPath.row == 9) {
				hCell.Hlbl1.text = historyText;
			}else if (indexPath.row == 10) {
				hCell.Hlbl1.text = historyText;
			}else if (indexPath.row == 11) {
				hCell.Hlbl1.text = historyText;
			}else if (indexPath.row == 12) {
				hCell.Hlbl1.text = historyText;
			}
		}
		
		return hCell;
	}else {
		UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
		if(cell == nil) {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
		}
		return cell;
	}
}

#pragma mark - ChangeToEnglish
- (void)ChangeToEnglish {
	LStatus = 1;
	historyText = @"The history of Kuravilangad and that of the Church which is its epicentre, though sketchy at the beginning and blank in middle years has since become sizeable. Two years – 105 and 335 of the Christian era - are reckoned to be significant milestones of her early period. The former is credited to be the year when the four Brahmin families converted by St Thomas – Kalli, Kalikavu, Pakalomattam and Sankarapuri – settled in the vicinity of Kuravilangad, following persecution at their hometown of Palayoor. A Fresh Group of migrants from Mylapore,Called Kadapoor Anchuveeder - Valiyaveedu, Kathadath, Chempukulam, Mancherry and Puthussery - later joined them. The latter year links Mother Mary and the Church dedicated in her name. According to tradition, the Virgin appeared in the guise of an old lady to some famished children who were tending their flock. To their great relief, the lady turned some pebbles into loaves and what\'s more, there arose a miraculous spring where she removed a handful of earth. The children ate and drank to their hearts\' content. Our Lady then disclosed her desire to have a Church built in her honour at the site. The elders were duly informed and eventually the Church was blessed in 345. \n \n \t In 1599, when Archbishop Dom Alexis Menezes of Goa visited Kuravilangad, \"he laid the foundation stone of a new Church ... as the old one, built of bamboo, was very dark and in the old style\" (Gouvea, Jornada of Dom Alexis de Menezes). The tenure of Parampil Palliveettil Chandy Kathanar as Vicar (1638-1663) saw the building of all the three churches in the present Marth Mariam complex. Mortar and laterite bricks were used for the first time in their construction. The sanctum as well as the southern and northern sacristies of the main Church were retained when the present church was built in the 1950s. The exquisitely carved baroque retable attached to the main altar is a veritable delight to the eyes. The Chapel of St Sebastian, located on a nearby hilltop and enclosed after the manner of a fortress, predates the main church by a few years. The third one, the cemetery chapel dedicated to St Joseph, was built at the direction of Mar Sebastiani. \n \n \t St Mary\'s Church has etched an inimitable spiritual mark of her own among believers by the sacred artefacts associated with her. The statue of the Blessed Virgin Mary kept at the southern sacristy is believed to be translated from North India in the 16th century when Christianity here was under severe persecution. Since then, the image of Kuravilangad Muthiyamma in the minds of devotees has been inextricably fused with this statue of Mother Mary holding child Jesus. The granite cross standing 48 feet tall, in front of the church, spiritually shields the whole town.   We read an impressive account of \"a solemn procession, at times accompanied by trumpets and tambourines, from the church to a most beautiful granite cross nearby with full of oil lamps at its pedestals\", in Prima Speditione All\'Indie Orientali of Giuseppe Santa Maria Sebastiani (1666) who visited Kuravilangad in 1657. Offering oil to the sacred lamps at the foot of the cross is a very old custom. Thousands in circular queues waiting for their turn around the mighty cross is an edifying sight on First Fridays. There is a tradition that Europeans brought to Kerala seven copies of \'Madonna and Child\' icon painted by the Evangelist St. Luke for use in seven ancient churches here and that Kuravilangad got one. Friar Vincenzo Maria who visited Kuravilangad in the 1650s, wrote in his book Il Viaggio all\' Indie  Orientali (1672) that in Kuravilangad  church there is one picture of St. Mary exactly similar to the one in Santa Maria Maggiore Basilica in Rome. This may be one reason why the Carmelite Missionaries called the church St Mary Major of Malabar. \n\n \t Kuravilangad can surely boast of her ecclesiastical luminaries. Palliveettil Chandy Methran (Bishop Alexander de Campo, 1613 –1687) was the first indigenous bishop from India.  He used the title of \'Metropolitan of All India\'. Chandy Methran was using the Valia palli (big church) as his Cathedral and the Cheria palli (small church) as his private chapel. He was buried in the Madbeha of the main church. Panamkuzhackal Valliachan (1479 – 1543) and Nidheerickal Manikkathanar (1842 – 1904) are two other prominent sons of the parish. They were also buried inside the church. Kuravilangad is also the home parish of the Archdeacons, who were the de facto leaders of St. Thomas Christians until the 16th century. Down the centuries, the quotidian affairs of the Church were in their able hands. The mortal remains of several archdeacons are still preserved at the Pakalomattom Tharavadu Chapel. \n\n \t Rogation of Ninevites or Moonu nombu, a moveable feast celebrated on Monday, Tuesday and Wednesday, three weeks prior to the beginning of Lent, is the main feast of the church. On the second day of the feast a replica of Jonah\'s vessel is taken out in procession. Thousands from far and near gather to witness this unique ritual.\n\n \t The museum of the church houses ancient manuscripts and articles of historic, artistic and spiritual significance. An ancient bell with the engraving in Syriac \"Mother of God\" (Emme de Alaha) is one of its prized possessions. \n\n \t Kuravilangad occupies a unique place today as an immense and lively parish. Membership crosses 15000 and no fewer than 2000 students undergo faith formation every year.";
	[HTableView reloadData];
}

#pragma mark - ChangeToMalayalam
- (void)ChangeToMalayalam {
	LStatus = 0;
	
	 body1 = @"സവിശേഷതകളേറെയുള്ള ഐതിഹ്യങ്ങളുടെയും പാരമ്പര്യങ്ങളുടെയും ചരിത്രസംഭവങ്ങളുടെയും സംഗമഭൂമിയാണ് കുറവിലങ്ങാട്‌. എ.ഡി 52-ല്‍ തോമ്മാശ്ലീഹാ ഭാരതത്തില്‍ എത്തിയപ്പോള്‍ പാലയൂരില്‍ ക്രിസ്തു മതം സ്വീകരിച്ചവരില്‍ പ്രമുഖരായിരുന്നത് കള്ളി, കാളികാവ്, ശങ്കരപുരി, പകലോമറ്റം എന്നീ നാല് കുടുംബക്കാര്‍ ആയിരുന്നു. പാലയൂരില്‍   എതിര്‍പ്പുകള്‍ നേരിട്ടപ്പോള്‍ അവിടെ നിന്നും പുറപ്പെട്ട്  അവര്‍ കുറവിലങ്ങാട് എത്തിച്ചേര്‍ന്നതോടെയാണ്  എ. ഡി 105-ല്‍ ഇവിടുത്തെ ക്രൈസ്തവസമൂഹം ആരംഭിക്കുന്നത്. എ.ഡി.335-ല്‍ പരി. ദൈവമാതാവിന്റെ ദര്‍ശനം ലഭിച്ചതോടെയാണ് കുറവിലങ്ങാട്ട് ഇപ്പോള്‍  സ്ഥിതിചെയ്യുന്ന ദൈവാലയം കേന്ദ്രീകരിച്ച് ആരാധനാ സമൂഹം വളരുന്നത്. ആടുകളെ  മേയ്ച്ചുകൊണ്ടു നടന്ന കുട്ടികള്‍ക്ക് പരിശുദ്ധ കന്യകാമറിയം മുത്തിയമ്മയുടെ രൂപത്തില്‍ (മുത്തശ്ശി) പ്രത്യക്ഷപ്പെട്ട് വിശപ്പകറ്റാന്‍ അപ്പം  നല്‍കുകയും  ദാഹമകറ്റാന്‍ നീരുറവ തെളിച്ചുകൊടുക്കുകയും ചെയ്തു. വിവരമറിഞ്ഞ് ഓടിയെത്തിയ മാതാപിതാക്കളോട് അവിടെ ഒരു ദൈവാലയം സ്ഥാപിക്കണമെന്ന് മാതാവ് ആവശ്യപ്പെട്ടു. അങ്ങനെയാണ് ഇപ്പോള്‍ കാണുന്ന വലിയ പള്ളിയുടെ സ്ഥാനത്ത് ആദ്യ ദൈവാലയം ഉണ്ടാകുന്നത്. എ.ഡി. 345-ല്‍ എദേസ്സയില്‍ നിന്നു വന്ന മാര്‍ യൗസേപ്പ് മെത്രാനാണ്  പരിശുദ്ധ മറിയത്തിന്റെ നിര്‍ദ്ദേശാനുസരണം നിര്‍മ്മിച്ച ദേവാലയത്തിന്റെ വെഞ്ചരിപ്പു കര്‍മ്മം നിര്‍വഹിച്ചത്. ഇതിന്റെ സ്ഥാനത്താണ് ഇപ്പോഴുള്ള തെക്കേ സങ്കീര്‍ത്തി. ഇവിടെയാണ് മുത്തിയമ്മയുടെ രൂപം പ്രതിഷ്ഠിച്ചിരിക്കുന്നത്. പിന്നീട് പലപ്രാവശ്യം ഈ ദൈവാലയം പുതുക്കിനിര്‍മ്മിച്ചു. 1599 ജൂണിനും നവംബറിനും  ഇടയ്ക്ക്  ഉദയംപേരൂര്‍ സൂനഹദോസിനോടനുബന്ധിച്ച് മെനേസിസ് മെത്രാപ്പോലീത്താ കുറവിലങ്ങാട് സന്ദര്‍ശിച്ചപ്പോഴാണ് കല്ലുകൊണ്ടുള്ള ദൈവാലയത്തിന്റെ ശിലാസ്ഥാപനം നടത്തിയത്. അതിനു മുമ്പുവരെ മുളകൊണ്ട് ക്ഷേത്രങ്ങളുടെ  രീതിയില്‍ നിര്‍മ്മിച്ച ദൈവാലയമായിരുന്നു നിലവിലിരുന്നത് (Antonio de Gouvea, Jornada do Archbispo, 1603) പറമ്പില്‍ ചാണ്ടിക്കത്തനാര്‍ വികാരിയായിരുന്ന  കാലത്താണ് (1663 വരെ) ഇപ്പോഴുണ്ടായിരുന്ന ദൈവാലായത്തിന് മുന്‍പുണ്ടായിരുന്ന ദൈവാലയം നിര്‍മ്മിച്ചത്. 1954-1960 കാലഘട്ടത്തിലാണ് പുതിയ ദൈവാലയം നിര്‍മ്മിച്ചത്. പഴയപള്ളിയുടെ മദ്ബഹയും തെക്കും വടക്കുമുള്ള സങ്കീര്‍ത്തികളും  മാണിക്കത്തനാര്‍ ഉപയോഗിച്ചിരുന്ന മുറിയും നിലനിര്‍ത്തിക്കൊണ്ടാണ് പുതിയ ദൈവാലയം പണിതത്.  വി.പത്രോസ്, വി.തോമ്മാ എന്നീ ശ്ലീഹന്മാരുടെ പൂര്‍ണ്ണകായ പ്രതിമകള്‍ പള്ളിയുടെ രണ്ട് ഗോപുരങ്ങള്‍ അലങ്കരിക്കുന്നു. പള്ളിയുടെ നിര്‍മ്മാണത്തിന് നേതൃത്വം നല്‍കിയത് ഫാ.തോമസ് മണക്കാട്ടാണ്.";
	 heading2 = @"ഉപദേവാലയങ്ങള്‍";
	 body2 = @"കുറവിലങ്ങാട് വലിയ പള്ളികൂടാതെ വിശുദ്ധ സെബസ്ത്യാനോസിന്റെ നാമധേയത്തില്‍ ഒരു ചെറിയ പള്ളിയും, സിമിത്തേരിയില്‍ വിശുദ്ധ ഔസേഫിന്റെ നാമധേയത്തില്‍ മറ്റൊരു പള്ളിയുമുണ്ട്. എ.ഡി. 1653 ലെ കൂനന്‍കുരിശു സത്യത്തിന് ശേഷം അപ്പസ്‌തോലിക സന്ദര്‍ശനത്തിനായി ഇവിടെ വന്ന്, ഇവിടെ വികാരിയായിരുന്ന പള്ളിവീട്ടില്‍ ചാണ്ടി കത്തനാരെ കേരളത്തിലെ ഒന്നാമത്തെ നാട്ടുമെത്രനായി വാഴിച്ച  മാര്‍ സെബസ്ത്യാനിയോടുള്ള നന്ദി സൂചിപ്പിക്കാനാണ് അദ്ദേഹത്തിന്റ നാമഹേതുകയായ വിശുദ്ധ സെബസ്ത്യാനോസിന്റെ നാമത്തിലുള്ള ദേവാലയം ഇന്നാട്ടുകാര്‍ ഇന്നും പരിപാവനമായി സൂക്ഷിക്കുന്നത്. ജനുവരി 19, 20 തീയതികളില്‍ ആഘോഷിക്കുന്ന ചെറിയപള്ളിയിലെ തിരുനാള്‍ പത്താം തീയതി പെരുന്നാള്‍ എന്നറിയപ്പെടുന്നു.";
	 heading3 = @"പ്രധാന അള്‍ത്താര";
	 body3 = @"പോര്‍ച്ചുഗീസ് ബറോക് കലാവൈദഗ്ദ്ധ്യം വിളിച്ചോതുന്നതാണ് വലിയ പള്ളിയുടെ മദ്ബഹാ. ആറ് വലിയ തിരുസ്വരൂപമാണ് ഇവിടെ  പ്രതിഷ്ഠിച്ചിട്ടുള്ളത്. സ്വര്‍ഗ്ഗോരോപിതയായ പരി. കന്യകാമറിയം, വി. യോഹന്നാന്‍ മാംദാനാ, വി. തോമ്മാ, വി. പത്രോസ്, വി. പൗലോസ്, കര്‍മ്മലമാതാവ് എന്നി തിരുസ്വരൂപങ്ങളാണ് പ്രധാന അള്‍ത്താരയിലുള്ളത്. 1642-ല്‍ കുറവിലങ്ങാട്ട് ആരംഭിച്ചതും അയ്യായിരത്തില്‍പ്പരം അംഗങ്ങളാല്‍ സമ്പന്നവുമായിരുന്ന മലബാറിലെ കര്‍മ്മലമാതാവിന്റെ ആദ്യ ഉത്തരീയ സഭയുടെ  സ്വാധീനമാണ് കര്‍മ്മലമാതാവിന്റെ രൂപം ഇവിടെ പ്രതിഷ്ഠിച്ചതിനു പിന്നില്‍. (Vincent Mary, Viaggio all\'Indie Orientali, Venice, 1683, p 172)";
	 heading4 = @"മുത്തിയമ്മയുടെ രൂപം";
	 body4 = @"കുറവിലങ്ങാട് പള്ളിയുടെ പ്രധാന അള്‍ത്താരയുടെ തെക്കുവശത്തുള്ള ചെറിയ അള്‍ത്താരയില്‍ ഉണ്ണീശോയെ കൈയ്യിലേന്തിയ കന്യാമറിയത്തിന്റെ മനോഹരമായ ഒരു കരിങ്കല്‍ പ്രതിമയുണ്ട്. ഈ രൂപത്തിന് ഒരു ചരിത്രമുണ്ട്. 16 ാം നൂറ്റാണ്ടില്‍ വടക്കേഇന്ത്യയില്‍ ക്രിസ്തുമതം നശിപ്പിക്കപ്പെട്ടപ്പോള്‍ അവിടെയുണ്ടായിരുന്ന തകര്‍ക്കപ്പെട്ട ഒരു പള്ളിയില്‍ നിന്നും കടലില്‍കൂടി ഒഴുകി കടുത്തുരുത്തിയില്‍ എത്തിയ രൂപം അവിടെനിന്ന് ഇവിടെ കൊണ്ടുവന്ന് സ്ഥാപിച്ചതാണ്. മുക്തിപ്രാപിച്ച അമ്മ എന്ന അര്‍ത്ഥത്തിലാണ് മുത്തിയമ്മ എന്ന് വിളിക്കുന്നത്.";
	 heading5 = @"അത്ഭുത ഉറവ";
	 body5 = @"പരിശുദ്ധ കന്യകാമറിയം (കുറവിലങ്ങാട് മുത്തിയമ്മ) എ. ഡി.  335-ല്‍ പ്രത്യക്ഷപ്പെട്ട് ദാഹിച്ചു വലഞ്ഞ കുട്ടികള്‍ക്ക് കാണിച്ചുകൊടുത്ത അത്ഭുത ഉറവ പള്ളിയുടെ കിഴക്കുഭാഗത്ത് ഇപ്പോഴുമുണ്ട്. അനേകര്‍ക്ക് അത്ഭുതരോഗശാന്തി നല്‍കിക്കൊണ്ടിരിക്കുന്ന തീര്‍ത്ഥമാണിതെന്നതിന് തുടര്‍ച്ചയായി ലഭിക്കുന്ന സാക്ഷ്യങ്ങള്‍ തെളിവാണ്. നാനാജാതിമതസ്ഥരായ അനേകര്‍ ഈ ജലം ശേഖരിക്കാന്‍ ഇവിടെ എത്തുന്നു. തൊട്ടിയും കയറും ഇവിടെ സമര്‍പ്പിക്കുന്നത് ഒരു പ്രധാന നേര്‍ച്ചയാണ്";
	 heading6 = @"കല്‍കുരിശ്";
	 body6 = @"ഉദ്ദിഷ്ഠകാര്യം സാധിക്കുന്നതിന് വിവിധ തരം നേര്‍ച്ചകളുണ്ട്. അതില്‍ പ്രധാനപ്പെട്ടതാണ് വിളക്കുവയ്ക്കല്‍, കല്‍ക്കുരിശിനു ചുറ്റും എണ്ണയൊഴിക്കുക എന്നിവ.  എല്ലാ ദിവസവും പ്രത്യേകിച്ച് ആദ്യ വെള്ളിയാഴ്ചകളില്‍ എണ്ണഴൊഴിക്കാന്‍ കാത്തുനില്‍ക്കുന്ന വിശ്വാസികളുടെ നീണ്ട നിര ദൃശ്യമാണ്. പള്ളിയ്ക്ക് മുന്‍പിലുള്ള കല്‍കുരിശിന് 48 അടി ഉയരമുണ്ട്. പള്ളിവീട്ടില്‍ ചാണ്ടിക്കത്തനാര്‍ വികാരിയായിരുന്നപ്പോള്‍ സ്ഥാപിച്ചതാണ് ഈ കല്‍കുരിശ്.";
	 heading7 = @"സുപ്രസിദ്ധമായ മണികള്";
	 body7 = @"ലോകപ്രശസ്തിയാര്‍ജ്ജിച്ച മണികളാണ് കുറവിലങ്ങാട് പള്ളിയിലുള്ളത്. കേരസഭാചരിത്രത്തെ സംബന്ധിച്ചിടത്തോളം പ്രാധാന്യമര്‍ഹിക്കുന്ന 1584 ല്‍ നിര്‍മ്മിച്ച ഒരു മണി കുറവിലങ്ങാട്ട് ഉണ്ട്. കേരളത്തില മാര്‍ത്തോമ ക്രിസ്ത്യാനികള്‍ നെസ്‌തോറിയന്മാര്‍ ആയിരുന്നില്ല എന്നതിനുള്ള ചരിത്ര സാക്ഷ്യങ്ങളിലൊന്നാണ് ഈ മണി. കാരണം ഈ മണിയില്‍ \'ദൈവത്തിന്റെ അമ്മയായ മറിയം\' എന്ന് എഴുതിയിട്ടുണ്ട്. ഇതുകൂടാതെ പോര്‍ച്ചുഗീസുകാര്‍ പോര്‍ച്ചുഗലില്‍ വാര്‍പ്പിച്ച് കുറവിലങ്ങാട് കൊണ്ടുവന്ന ഒരു മണിയുണ്ട്. അതിലെ ലിഖിതം ഇന്നോളം ആര്‍ക്കും വായിക്കാന്‍ കഴിഞ്ഞട്ടില്ല. അതില്‍ പൂര്‍ണ്ണ വാക്യങ്ങളില്ല. കുറെ അക്ഷരങ്ങള്‍മാത്രം. ഏതെങ്കിലും പ്രാര്‍ത്ഥനാവാക്യങ്ങളുടെ ആദ്യാക്ഷരങ്ങളാകാം. ഇപ്പോഴത്തെ മണിമാളികയില്‍ കാണുന്ന മൂന്നു വലിയ മണികള്‍ 1910 ല്‍ ജര്‍മ്മനിയില്‍ നിര്‍മ്മിച്ചവയാണ്. മൂന്നു മണികളും മൂന്നു വലിപ്പത്തിലുള്ളവയും ആട്ടി അടിയ്ക്കാവുന്നതുമാണ്.  വലിയ മണിക്ക് ഒരാള്‍ പൊക്കവും അതിനൊത്ത വലിപ്പവുമുണ്ട്. ഏഷ്യയിലെ ഏറ്റവും വലിയ പള്ളിമണികളാണ് ഇവ. ";
	 heading81 = @"തിരുനാളുകള്‍";
	 heading82 = @"മൂന്നുനോമ്പ് തിരുനാള്";
	 body8 = @"കുറവിലങ്ങാട് പള്ളിയിലെ ഏറ്റവും പ്രധാന തിരുനാളാണ് മൂന്നുനോമ്പ്. യോനാ പ്രവാചകന്റെ മാനസാന്തരത്തിന്റെ ഓര്‍മ്മയുണര്‍ത്തുന്നതാണ് ഈ തിരുനാള്‍. \'നിനവേക്കാരുടെ യാചന\' എന്നും അറിയപ്പെടുന്ന ഈ നോമ്പ്  6ാം നൂറ്റാണ്ടുമുതല്‍ പൗരസ്ത്യ സുറിയാനി സഭയില്‍ പ്രചാരത്തിലുണ്ട്. മാര്‍ത്തോമ്മാ നസ്രാണികളും പരമ്പരാഗതമായി ഈ നോമ്പ് ആചരിച്ചിരുന്നതായി ആര്‍ച്ച് ബിഷപ് ഫ്രാന്‍സിസ് റോസ് 1612-ല്‍ എഴുതിയ കത്ത് സാക്ഷിക്കുന്നു.  ഫ്രെബുവരി 2-ാം തീയതിയിലെ പരിശുദ്ധ കന്യകാമറിയത്തിന്റെ ശുദ്ധീകരണ തിരുനാളും മൂന്നുനോമ്പു തിരുനാളിനേട് അനുബന്ധിച്ചാണ് കുറവിലങ്ങാട്ട് കൊണ്ടാടുന്നത്.  നോമ്പും ആഘോഷവും ഇങ്ങനെ സമാഗമിക്കുന്നു. പണ്ടുകാലത്ത് മൂന്നുദിവസം കുടില്‍ കെട്ടി ദേവാലയത്തിന് ചുറ്റും താമസിച്ച് തിരുനാളിലും നോമ്പിലും പങ്കെടുക്കുന്ന പാരമ്പര്യമുണ്ടായിരുന്നു. ഇന്നും നാനാജാതി മതസ്ഥരായ അനേകം പേര്‍ ദൂരദേശത്തുനിന്നുപോലും ഈ ദിനങ്ങളില്‍ ഇവിടെയെത്തി താമസിച്ച് മൂന്നു ദിവസത്തെയും തിരുകര്‍മ്മങ്ങളില്‍  പങ്കെടുക്കുന്നു.  വലിയനോമ്പിന് 18 ദിവസം മുന്‍പുള്ള തിങ്കള്‍, ചൊവ്വ, ബുധന്‍ ദിവസങ്ങളിലാണ് മൂന്നുനോമ്പ് തിരുനാള്‍.";
	 heading9 = @"കപ്പല്‍ പ്രദക്ഷിണം";
	 body9 = @"മൂന്നുനോമ്പിന്റെ ചൊവ്വാഴ്ചയാണ് ലോകപ്രശസ്തമായ കപ്പല്‍ പ്രദക്ഷിണം. ദൈവത്തിന്റെ വാക്ക് ധിക്കരിച്ചുള്ള യോനാപ്രവാചകന്റെ കപ്പല്‍ യാത്രയും കടല്‍ക്ഷോഭവും തുടര്‍ന്ന് യോനായെ കടലില്‍ എറിയുന്നതും കടല്‍ ശാന്തമാകുന്നതുമാണ് കപ്പല്‍ പ്രദക്ഷിണത്തില്‍ ദൃശ്യവത്കരിക്കുന്നത്. പരമ്പരാഗതമായി കടപ്പൂര്‍ നിവാസികളാണ് പ്രദക്ഷിണത്തിനായി കപ്പല്‍ എടുക്കുന്നത്. നൂറ്റാണ്ടുകള്‍ക്കുമുമ്പ് കപ്പല്‍ വ്യാപാരത്തിലേര്‍പ്പെട്ടിരുന്ന കടപ്പൂര്‍ നിവാസികള്‍ സഞ്ചരിച്ച കപ്പല്‍ ഒരിക്കല്‍ ശക്തമായ കടല്‍ക്ഷോഭത്തില്‍പെട്ടു. അപ്പോള്‍ കുറവിലങ്ങാട് മുത്തിയമ്മയെ വിളിച്ചു പ്രാര്‍ത്ഥിക്കുകയും ഒരു കപ്പലിന്റെ മാതൃക പണികഴിപ്പിച്ച് കുറവിലങ്ങാട് പള്ളിക്ക് കൊടുത്തുകൊള്ളാമെന്ന് നേര്‍ച്ച നേരുകയും ചെയ്തതോടെ കടല്‍ ശാന്തമായി. സുരക്ഷിതരായി തിരിച്ചെത്തിയ അവര്‍ കപ്പല്‍ നിര്‍മ്മിച്ച് കുറവിലങ്ങാട് പള്ളിക്ക് കൊടുത്തു. മൂന്നുനോമ്പിന്റെ അനുഷ്ഠാനങ്ങളില്‍ പ്രധാനപ്പെട്ടതായി കാലക്രമത്തില്‍ കപ്പല്‍ പ്രദക്ഷിണം മാറി.";
	 heading10 = @"പത്താം തീയതി തിരുനാള്‍";
	 body10 = @"വി.സെബസ്ത്യാനോസിന്റെ തിരുനാളാണ് പത്താം തീയതി തിരുനാള്‍ എന്ന പേരില്‍ ആചരിക്കുന്നത്.  ജൂലിയന്‍ കലണ്ടര്‍ അനുസരിച്ച് വിശുദ്ധന്റെ തിരുനാളായ ജനുവരി 20 എന്നത് 10-ാം തീയതി ആയതിനാലാണ് ഇങ്ങനെ ഒരു പേരുവന്നത്. മൂന്നുനോമ്പിന്റെ തീയതികള്‍  ജനുവരി 20 ന് അടുത്ത് വരുമ്പോള്‍  വലിയ നോമ്പിന് മുമ്പുള്ള ശനി, ഞായര്‍ ദിവസങ്ങളിലാണ് ഈ തിരുനാള്‍ ആഘോഷിക്കുന്നത്  ചെറിയ പെരുന്നാള്‍, ചെറിയ പള്ളിപ്പെരുന്നാള്‍ എന്നൊക്കെ ഈ തിരുനാള്‍ പ്രസിദ്ധമാണ്. കഴുന്ന് (അമ്പ്) വീടുകളില്‍ ആഘോഷപൂര്‍വ്വം പ്രതിഷ്ഠിക്കുന്നതും തുടര്‍ന്ന് പ്രദക്ഷിണമായി ഇടവകയുടെ നാനാഭാഗങ്ങളില്‍ നിന്നും പള്ളിയില്‍ എത്തിക്കുന്നതും ഈ തിരുനാളിന്റെ മുഖ്യ സവിശേഷതയാണ്. ദാരിദ്ര്യം, കഷ്ടനഷ്ടങ്ങള്‍, പകര്‍ച്ചവ്യാധികള്‍, പടുമരണങ്ങള്‍ ഇവയില്‍ നിന്നൊക്കെ രക്ഷനേടുവാന്‍ സെബസ്ത്യാനോസിന്റെ മാദ്ധ്യസ്ഥം തേടുവാന്‍ ആയിരങ്ങള്‍ ഇവിടെ എത്തിച്ചേരുന്നു.";
	 heading11 = @"എട്ടുനോമ്പ് തിരുനാള്‍";
	 body11 = @"മാര്‍ത്തോമ്മാ ക്രിസ്ത്യാനികളുടെ പരമ്പരാഗതമായ നോമ്പാണ് സെപ്റ്റംബര്‍ 1 മുതല്‍ 8 വരെയുള്ള എട്ടുനോമ്പ്. പരി.കന്യകാമറിയത്തിന്റെ ജനനത്തിരുനാളിനോടനുബന്ധിച്ചാണ് ഇത് ആചരിക്കുന്നത്. സെപ്റ്റംബര്‍ 8 നു മുമ്പുള്ള സപ്തദിനങ്ങള്‍ മുഴുവന്‍ പരിശുദ്ധമായി ആചരിക്കുന്നു. ഈ ദിവസങ്ങളില്‍ കുട്ടികളും മുതിര്‍ന്നവരും മുത്തിയമ്മയുടെ രൂപത്തിങ്കല്‍ പൂക്കള്‍ അര്‍പ്പിക്കുന്നതും  രാപകല്‍  ദൈവാലയത്തിലിരുന്നു പ്രാര്‍ത്ഥിക്കുന്നതും ഈ തിരുനാളിന്റെ പ്രത്യേകതയാണ്.";
	 heading12 = @"പകലോമറ്റം തറവാട് പള്ളി";
	 body12 = @"മാര്‍ത്തോമ്മാശ്ലീഹായില്‍നിന്നും മാമ്മോദീസ സ്വീകരിച്ച് പാലയൂരില്‍ നിന്ന് എത്തിയ കള്ളി, കാളികാവ്, ശങ്കരപുരി, പകലോമറ്റം എന്നീ കുടുംബങ്ങള്‍ എ.ഡി. 105 ല്‍ കുറവിലങ്ങാട്ട് കാളികാവില്‍ (പള്ളിക്കവലയില്‍ നിന്നും ഒന്നേ കാല്‍ കിലോമീറ്ററില്‍ തെക്കുമാറി) എത്തി താമസം തുടങ്ങി. ഇതില്‍ പകലോമറ്റം കുടുംബത്തില്‍ നിന്നാണ് ജാതിക്ക് കര്‍ത്തവ്യന്മാരായ അര്‍ക്കദിയാക്കോന്മാരെ തെരഞ്ഞെടുത്തിരുന്നത്. പകലോമറ്റം മൂലതറവാടിനോട് ചേര്‍ന്നാണ് അര്‍ക്കദിയാക്കോന്മാരെ സംസ്‌കരിച്ചത്. മൂലതറവാടിന്റെ സ്ഥാനത്താണ് ഇന്നുകാണുന്ന തറവാടുപള്ളി. 1953-ല്‍ ഈ സ്ഥലം പള്ളിക്ക് ദാനമായി ലഭിക്കുകയും പകലോമറ്റം കുടുംബക്കാരുടെ സഹകരണത്തോടെ 1963 ല്‍ കപ്പേളയും കല്‍കുരിശും സ്ഥാപിക്കുകയും ചെയ്തു. കപ്പേള പണിയുന്ന കാലത്ത് പഴയ നാലുകെട്ടിന്റെ തറകള്‍ കാണാമായിരുന്നു. വീടിനോട് ചേര്‍ന്നുണ്ടായിരുന്ന കിണര്‍ ഇന്നും കാണാം. ഈ കപ്പേള 2009 ല്‍ \'തറവാടുപള്ളി\' എന്ന പേരില്‍ പുതുക്കിപണിതു.‍";
	
	historyText = [NSString stringWithFormat:@"%@ \n\n\n%@\n\n%@ \n\n\n%@\n\n%@ \n\n\n%@\n\n%@ \n\n\n%@\n\n%@ \n\n\n%@\n\n%@ \n\n\n%@\n\n%@ \n\n\n%@\n\n%@\n\n%@ \n\n\n%@\n\n%@ \n\n\n%@\n\n%@ \n\n\n%@\n\n%@ \n\n\n%@\n\n%@",body1, heading2, body2, heading3, body3, heading4, body4, heading5, body5, heading6, body6, heading7, body7, heading81, heading82, body8, heading9, body9, heading10, body10, heading11, body11, heading12, body12];
	

	[HTableView reloadData];
}

@end
