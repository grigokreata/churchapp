//
//  ServerClass.m
//  ChurchApp
//
//  Created by Grigo Mathews on 30/01/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import "ServerClass.h"
#import "Reachability.h"
#import "GlobalIdentifiers.h"
#import "AppDelegate.h"

@implementation ServerClass
@synthesize delegateSCD;

- (BOOL)connected {
	Reachability *reachability = [Reachability reachabilityForInternetConnection];
	NetworkStatus networkStatus = [reachability currentReachabilityStatus];
	return !(networkStatus == NotReachable);
}


#pragma mark - sendServerRequest
- (void)GetServerRequest:(NSString *)apiName {
	//URlRequest object and its initialization
	NSURL *apiURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWebService_BASEURL,apiName]];
	NSLog(@"GetServerRequest: %@",apiURL);
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:apiURL];
	[request setTimeoutInterval:30];
	[request setHTTPMethod:@"GET"];
	NSURLSessionConfiguration *sessionConfig =  [NSURLSessionConfiguration defaultSessionConfiguration];
	NSURLSession *session =  [NSURLSession sessionWithConfiguration:sessionConfig
														   delegate:self
													  delegateQueue:[NSOperationQueue mainQueue]];
	NSURLSessionDataTask *jsonData = [session dataTaskWithRequest:request];
	[jsonData resume];
	
	if (session) {
		webData = [[NSMutableData alloc]init];
	}
}

- (void)PostServerRequest:(NSString *)apiName Parameters:(NSString *)parameters {
	
	NSData *postData = [parameters dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
	//URlRequest object and its initialization
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kWebService_BASEURL,apiName]];
	NSLog(@"PostServerRequestURL: %@",url);
	NSLog(@"PostServerRequestParameters: %@",parameters);
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
	[request setTimeoutInterval:30];
	[request setHTTPMethod:@"POST"];
	[request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[postData length]] forHTTPHeaderField:@"Content-Length"];
	[request setValue:@"application/x-www-form-urlencoded;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
	[request setHTTPBody:postData];
	
	NSURLSessionConfiguration *sessionConfig =  [NSURLSessionConfiguration defaultSessionConfiguration];
	NSURLSession *session =  [NSURLSession sessionWithConfiguration:sessionConfig
														   delegate:self
													  delegateQueue:[NSOperationQueue mainQueue]];
	NSURLSessionDataTask *jsonData = [session dataTaskWithRequest:request];
	[jsonData resume];
	
	if (session) {
		webData = [[NSMutableData alloc]init];
	}
}

#pragma mark - NSURLSessionDelegate Methods
- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask didReceiveResponse:(NSURLResponse *)response
 completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler {
	webData = nil;
	webData = [[NSMutableData alloc] init];
	[webData setLength:0];
	completionHandler(NSURLSessionResponseAllow);
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask  didReceiveData:(NSData *)data {
	[webData appendData:data];
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error {
	if (error) {
		NSLog(@"\n\n ServerClassError :%@\n\n",error);
	}else {
		if (delegateSCD) {
			[delegateSCD ServerCallBackResponse:webData];
		}
	}
}

#pragma mark - NSUTF8StringEncoding
- (NSString *)encodeString:(NSString *)string {
	NSString *newString = [string stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
	if (newString){
		return newString;
	}else {
		return @"";
	}
}

#pragma mark - AddUserDefaults
- (void)AddUserDefaults:(id)aobject Key:(NSString *)akey {
	AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
	[[delegate userDefaultsFile]setObject:aobject forKey:akey];
	
	[[delegate userDefaultsFile]synchronize];
	NSLog(@"AddUserDefaultsKey: %@, Value: %@", akey, [[delegate userDefaultsFile ]objectForKey:akey]);
}
#pragma mark - FetchUserDefaults
- (NSString *)FetchUserDefaults:(NSString *)fkey {
	AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
	NSString *fetchedData = [[delegate userDefaultsFile ]objectForKey:fkey];
	NSLog(@"UserDefaults fetched          %@ = %@", fkey, [[delegate userDefaultsFile ]objectForKey:fkey]);
	return fetchedData;
}
@end
