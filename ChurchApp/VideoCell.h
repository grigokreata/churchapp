//
//  VideoCell.h
//  ChurchApp
//
//  Created by Grigo Mathews on 25/02/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIWebView *VideoWebView;

@end
