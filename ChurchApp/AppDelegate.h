//
//  AppDelegate.h
//  ChurchApp
//
//  Created by Grigo Mathews on 30/01/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NSUserDefaults *userDefaultsFile;

@end

