//
//  NewsCell.h
//  ChurchApp
//
//  Created by Grigo Mathews on 01/02/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *CardView;
@property (weak, nonatomic) IBOutlet UIImageView *NewsImage;
@property (weak, nonatomic) IBOutlet UILabel *NewsLbl1;
@end
