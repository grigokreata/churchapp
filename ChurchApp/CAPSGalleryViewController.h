//
//  CAPSGalleryViewController.h
//  ChurchApp
//
//  Created by Grigo Mathews on 23/02/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CAPSGalleryDelegate <NSObject>

- (void)SelectedGalleryRowAtIndexPath:(NSString *)SelectedId AlbumName:(NSString *)albumname;

@end

@interface CAPSGalleryViewController : UIViewController

@property (strong, nonatomic) NSArray  *CAPSGalleryArray;

@end
