//
//  PhotoListViewController.h
//  ChurchApp
//
//  Created by Grigo Mathews on 27/02/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoListViewController : UIViewController

@property (strong, nonatomic) NSString *passingAlbumId;
@property (strong, nonatomic) NSString *passingName;

@end
