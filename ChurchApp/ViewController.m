//
//  ViewController.m
//  ChurchApp
//
//  Created by Grigo Mathews on 30/01/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import "ViewController.h"
#import "ServerClass.h"
#import "UIColor+GlobalColor.h"
#import "MenuCell.h"
#import "HistoryViewController.h"
#import "HolyBibleViewController.h"
#import "CAPSPageMenuViewController.h"
#import "PrayerBookViewController.h"
#import "NewsViewController.h"
#import "EventsViewController.h"
#import "CAPSGalleryViewController.h"
#import "OfferingsViewController.h"
#import "ContactsViewController.h"

static NSString *MenuCellId = @"MenuCell";

@interface ViewController ()<UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *MenuCollectionView;

@end

@implementation ViewController
@synthesize MenuCollectionView;

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	[self preferredStatusBarStyle];
	self.navigationController.navigationBarHidden = YES;
	self.navigationController.interactivePopGestureRecognizer.delegate = (id<UIGestureRecognizerDelegate>)self;
//	[UIView animateWithDuration:2.5
//						  delay:0.0
//						options: UIViewAnimationOptionCurveEaseInOut
//					 animations:^{
						 MenuCollectionView.frame = CGRectMake(0, 0, 320, 460);
//					 }
//					 completion:^(BOOL finished) {
//					 }];
	[self.view addSubview:MenuCollectionView];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
	return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
	return 8;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath  {
	
	MenuCell *mCell		= (MenuCell *)[collectionView dequeueReusableCellWithReuseIdentifier:MenuCellId forIndexPath:indexPath];
	
	if (indexPath.row == 0) {
		mCell.mCellImage.image = [UIImage imageNamed:@"1history"];
	}else if (indexPath.row == 1) {
		mCell.mCellImage.image = [UIImage imageNamed:@"2bible"];
	}else if (indexPath.row == 2) {
		mCell.mCellImage.image = [UIImage imageNamed:@"3prayer_book"];
	}else if (indexPath.row == 3) {
		mCell.mCellImage.image = [UIImage imageNamed:@"4news"];
	}else if (indexPath.row == 4) {
		mCell.mCellImage.image = [UIImage imageNamed:@"5calendar"];
	}else if (indexPath.row == 5) {
		mCell.mCellImage.image = [UIImage imageNamed:@"6gallery"];
	}else if (indexPath.row == 6) {
		mCell.mCellImage.image = [UIImage imageNamed:@"7offering"];
	}else if (indexPath.row == 7) {
		mCell.mCellImage.image = [UIImage imageNamed:@"8contact.png"];
	}
	return mCell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
	CGFloat picDimension_width  = (self.view.frame.size.width / 3.0f) - 45.0f;
//	CGFloat picDimension_height = (self.view.frame.size.height / 4.0f);
//	NSLog(@" picDimension_width  = %f",picDimension_width);
//	NSLog(@" picDimension_height = %f",picDimension_height);
	return CGSizeMake(picDimension_width, picDimension_width);
//	return CGSizeMake(80.0f, 80.0f);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
	return UIEdgeInsetsMake(10, 10, 10, 10);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath  {
	NSLog(@"didSelectItemAtIndexPath:");
	if (indexPath.row == 0) {
		UIStoryboard *mystoryboard				= [UIStoryboard storyboardWithName:@"Main" bundle:nil];
		HistoryViewController *HistoryVC		= [mystoryboard instantiateViewControllerWithIdentifier:@"HistoryViewController"];
		[self.navigationController pushViewController:HistoryVC animated:YES];
	}else if (indexPath.row == 1) {
		UIStoryboard *mystoryboard				= [UIStoryboard storyboardWithName:@"Main" bundle:nil];
		HolyBibleViewController *HolyBibleVC	= [mystoryboard instantiateViewControllerWithIdentifier:@"HolyBibleViewController"];
		[self.navigationController pushViewController:HolyBibleVC animated:YES];
	}else if (indexPath.row == 2) {
		UIStoryboard *mystoryboard				= [UIStoryboard storyboardWithName:@"Main" bundle:nil];
		CAPSPageMenuViewController *CAPSPageMenu= [mystoryboard instantiateViewControllerWithIdentifier:@"CAPSPageMenuViewController"];
		CAPSPageMenu.CAPSPrayerArray = @[@"Paryer Book", @"Liturgical Calendar"];
		[self.navigationController pushViewController:CAPSPageMenu animated:YES];
	}else if (indexPath.row == 3) {
		UIStoryboard *mystoryboard				= [UIStoryboard storyboardWithName:@"Main" bundle:nil];
		NewsViewController *NewsVC				= [mystoryboard instantiateViewControllerWithIdentifier:@"NewsViewController"];
		[self.navigationController pushViewController:NewsVC animated:YES];
	}else if (indexPath.row == 4) {
		UIStoryboard *mystoryboard				= [UIStoryboard storyboardWithName:@"Main" bundle:nil];
		EventsViewController *EventsVC			= [mystoryboard instantiateViewControllerWithIdentifier:@"EventsViewController"];
		[self.navigationController pushViewController:EventsVC animated:YES];
	}else if (indexPath.row == 5) {
		UIStoryboard *mystoryboard				= [UIStoryboard storyboardWithName:@"Main" bundle:nil];
		CAPSGalleryViewController *GalleryVC		= [mystoryboard instantiateViewControllerWithIdentifier:@"CAPSGalleryViewController"];
        GalleryVC.CAPSGalleryArray = @[@"PHOTOS", @"VIDEOS", @"LIVE"];
		[self.navigationController pushViewController:GalleryVC animated:YES];
	}else if (indexPath.row == 6) {
		UIStoryboard *mystoryboard				= [UIStoryboard storyboardWithName:@"Main" bundle:nil];
		OfferingsViewController *OfferingsVC	= [mystoryboard instantiateViewControllerWithIdentifier:@"OfferingsViewController"];
		[self.navigationController pushViewController:OfferingsVC animated:YES];
	}else if (indexPath.row == 7) {
		UIStoryboard *mystoryboard				= [UIStoryboard storyboardWithName:@"Main" bundle:nil];
		ContactsViewController *ContactsVC		= [mystoryboard instantiateViewControllerWithIdentifier:@"ContactsViewController"];
		[self.navigationController pushViewController:ContactsVC animated:YES];
	}
}

- (void)prefix_addUpperBorder {
	CALayer *upperBorder = [CALayer layer];
	upperBorder.backgroundColor = [[UIColor headerBgColor] CGColor];
	upperBorder.frame = CGRectMake(0, 0, CGRectGetWidth(self.MenuCollectionView.frame), 2.0f);
	[MenuCollectionView.layer addSublayer:upperBorder];
}
@end
