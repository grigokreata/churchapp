//
//  EventsViewController.m
//  ChurchApp
//
//  Created by Grigo Mathews on 31/01/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import "EventsViewController.h"
#import "ServerClass.h"
#import "GlobalIdentifiers.h"

@interface EventsViewController ()<ServerClassDelegate>
{
	ServerClass *serverclass;
	NSArray *EventsArray;
	NSArray *dateArray;
}

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (IBAction)BackBtnAction:(id)sender;

@end

@implementation EventsViewController
@synthesize indicator;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	serverclass = [[ServerClass alloc] init];
	NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd"];
	dateArray = [[dateFormatter stringFromDate:[NSDate date]] componentsSeparatedByString:@"-"];
	
	NSLog(@"TodayDate: %@",dateArray);
	[self ServerRequestEvents];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)BackBtnAction:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - WebService Methods
-(void)ServerRequestEvents {
	serverclass.delegateSCD = self;
//	NSString *payLoadString = [NSString stringWithFormat:@"app_id=%@&month=2&year=2017",kAppId];
	NSString *payLoadString = [NSString stringWithFormat:@"app_id=%@&month=%@&year=%@", kAppId, dateArray[1], dateArray[0]];
	NSString *apiName		= [NSString stringWithFormat:@"eventapi.php"];
	[serverclass PostServerRequest:apiName Parameters:payLoadString];
	[self processStart];
}

-(void)ServerCallBackResponse:(NSData *)serverResponse {
	[self processEnd];
	NSError *error = nil;
	NSData *objectData  = [[[NSString alloc] initWithData:serverResponse encoding:NSUTF8StringEncoding] dataUsingEncoding:NSUTF8StringEncoding];
	NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:objectData
															 options:NSJSONReadingAllowFragments
															   error:&error];
	if (!jsonDict) {
		NSLog(@"Error parsing JSON: %@", error);
	}else {
		NSLog(@"JSON: %@", jsonDict);
		if ([[jsonDict objectForKey:@"status"] isEqualToString:@"success"]) {
			EventsArray = [jsonDict objectForKey:@"eventdata"];
		}
	}
}

#pragma mark - UIActivityIndicator Methods
- (void)processStart {
	[indicator setHidden:NO];
	[indicator startAnimating];
}

- (void)processEnd {
	[indicator setHidden:YES];
	[indicator stopAnimating];
}

@end
