//
//  PhotoListViewController.m
//  ChurchApp
//
//  Created by Grigo Mathews on 27/02/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import "PhotoListViewController.h"
#import "PhotoCell.h"
#import "ServerClass.h"
#import "UIImageView+WebCache.h"
#import "GlobalIdentifiers.h"

static NSString *PhotoCellId = @"PhotoCell";

@interface PhotoListViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, ServerClassDelegate>
{
	NSArray *PhotoArray;
	ServerClass *serverclass;
}

@property (weak, nonatomic) IBOutlet UILabel *pTitle;
@property (weak, nonatomic) IBOutlet UICollectionView *PhotoCollectionView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

- (IBAction)BackBtnAction:(id)sender;

@end

@implementation PhotoListViewController
@synthesize passingAlbumId;
@synthesize passingName;
@synthesize pTitle;
@synthesize PhotoCollectionView;
@synthesize indicator;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	serverclass = [[ServerClass alloc] init];
	pTitle.text = passingName;
	[self ServerRequestPhotoList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)BackBtnAction:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
	return [PhotoArray count];
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath  {
	
	PhotoCell *pCell	= (PhotoCell *)[collectionView dequeueReusableCellWithReuseIdentifier:PhotoCellId forIndexPath:indexPath];
	pCell.photoImage.clipsToBounds = YES;
	NSURL *gImageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[[PhotoArray objectAtIndex:indexPath.row] objectForKey:@"image"]]];
	[pCell.photoImage sd_setImageWithURL:gImageUrl placeholderImage:[UIImage imageNamed:@"cross"]];
	return pCell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
	CGFloat picDimension_width  = (self.view.frame.size.width / 2.0f) - 10.0f;
//	CGFloat picDimension_height = (self.view.frame.size.height / 3.0f) + 15.0f;
	NSLog(@" picDimension_width  = %f",picDimension_width);
	//	NSLog(@" picDimension_height = %f",picDimension_height);
	//	return CGSizeMake(151, 178);
	return CGSizeMake(picDimension_width, picDimension_width);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
	return UIEdgeInsetsMake(5, 5, 5, 5);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath  {
//	NSLog(@"didSelectItemAtIndexPath: %@", [GalleryListArray objectAtIndex:indexPath.row]);
	//	if (delegateGD) {
	//		[delegateGD SelectedGalleryRowAtIndexPath:[[GalleryListArray objectAtIndex:indexPath.row] objectForKey:@"id"]];
	//	}
}

#pragma mark - WebService Methods
-(void)ServerRequestPhotoList {
	serverclass.delegateSCD = self;
	NSString *payLoadString = [NSString stringWithFormat:@"albumid=%@&offset=0&limit=9",passingAlbumId];
	NSString *apiName		= [NSString stringWithFormat:@"albumimages.php"];
	[serverclass PostServerRequest:apiName Parameters:payLoadString];
	[self processStart];
}

-(void)ServerCallBackResponse:(NSData *)serverResponse {
	[self processEnd];
	NSError *error = nil;
	NSData *objectData  = [[[NSString alloc] initWithData:serverResponse encoding:NSUTF8StringEncoding] dataUsingEncoding:NSUTF8StringEncoding];
	NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:objectData
															 options:NSJSONReadingAllowFragments
															   error:&error];
	if (!jsonDict) {
		NSLog(@"Error parsing JSON: %@", error);
	}else {
		NSLog(@"GalleryDetailArray JSON: %@", jsonDict);
		if ([[jsonDict objectForKey:@"status"] isEqualToString:@"success"]) {
			PhotoArray = [jsonDict objectForKey:@"album"];
			[PhotoCollectionView reloadData];
		}
	}
}

#pragma mark - UIActivityIndicator Methods
- (void)processStart {
	[indicator setHidden:NO];
	[indicator startAnimating];
}

- (void)processEnd {
	[indicator setHidden:YES];
	[indicator stopAnimating];
}

@end
