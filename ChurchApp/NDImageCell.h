//
//  NDImageCell.h
//  ChurchApp
//
//  Created by Grigo Mathews on 24/02/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NDImageCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *NImage;

@end
