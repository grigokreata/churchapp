//
//  HBDetailViewController.m
//  ChurchApp
//
//  Created by Grigo Mathews on 13/02/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import "HBDetailViewController.h"

@interface HBDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *hbdTitle;
@property (weak, nonatomic) IBOutlet UITextView *hbdtxt;

- (IBAction)BackBtnAction:(id)sender;

@end

@implementation HBDetailViewController
@synthesize passingTitle;
@synthesize passingText;
@synthesize hbdTitle;
@synthesize hbdtxt;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	hbdTitle.text = passingTitle;
	[hbdtxt setText:passingText];
//	self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)viewDidAppear:(BOOL)animated {
	
	[hbdtxt setContentOffset:CGPointZero];
//	[hbdtxt scrollRangeToVisible:NSMakeRange(0, 0)];
}

- (void)layoutSubviews{
//	[hbdtxt setContentOffset:CGPointZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)BackBtnAction:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

@end
