//
//  PrayerCell1.h
//  ChurchApp
//
//  Created by Grigo Mathews on 23/02/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrayerCell1 : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *heading1;
@property (weak, nonatomic) IBOutlet UILabel *heading2;
@property (weak, nonatomic) IBOutlet UILabel *heading3;
@property (weak, nonatomic) IBOutlet UILabel *body1;
@property (weak, nonatomic) IBOutlet UIButton *btn1;

@end
