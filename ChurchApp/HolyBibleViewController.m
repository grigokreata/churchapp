//
//  HolyBibleViewController.m
//  ChurchApp
//
//  Created by Grigo Mathews on 31/01/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import "HolyBibleViewController.h"
#import "UIColor+GlobalColor.h"
#import "HBChapterListViewController.h"
#import "GlobalIdentifiers.h"

@interface HolyBibleViewController ()<UITableViewDelegate, UITableViewDataSource>
{
	NSMutableArray *sBibleArray;
}
@property (weak, nonatomic) IBOutlet UITableView *bListTableView;

- (IBAction)BackBtnAction:(id)sender;

@end

@implementation HolyBibleViewController
@synthesize bListTableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	sBibleArray = [[NSMutableArray alloc] init];
	
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)BackBtnAction:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	switch (section) {
		case 0:
			return 45;
			break;
			
		case 1:
			return 27;
			break;
		default:
			return 0;
			break;
	}
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 81;
}


- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	switch (section) {
		case 0:
			return @"പഴയ നിയമം";
			break;
		case 1:
			return @"പുതിയ നിയമം";
			break;
		default:
			return @"";
			break;
	}
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	return 30;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	
	CGFloat width = self.view.frame.size.width;
	UILabel *myLabel	= [[UILabel alloc] init];
	myLabel.frame		= CGRectMake(0, 0, width, 30);
	myLabel.font		= [UIFont fontWithName:@"Thiruvachanam" size:15.0];
	myLabel.textColor	= [UIColor whiteColor];
	myLabel.textAlignment = NSTextAlignmentCenter;
	myLabel.text		= [self tableView:tableView titleForHeaderInSection:section];
	
	UIView *headerView	= [[UIView alloc] init];
	headerView.backgroundColor = [UIColor headerBgColor];
	[headerView addSubview:myLabel];
	
	return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *cellIdentifier = @"cellIdentifier";
	
	UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	if (cell == nil) {
		cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
		cell.textLabel.font = [UIFont fontWithName:@"Thiruvachanam" size:18.0f];
	}
	if (indexPath.section == 0) {
		switch (indexPath.row) {
			case 0:
				cell.textLabel.text = kOld1M;
				break;
			case 1:
				cell.textLabel.text = kOld2M;
				break;
			case 2:
				cell.textLabel.text = kOld3M;
				break;
			case 3:
				cell.textLabel.text = kOld4M;
				break;
			case 4:
				cell.textLabel.text = kOld5M;
				break;
			case 5:
				cell.textLabel.text = kOld6M;
				break;
			case 6:
				cell.textLabel.text = kOld7M;
				break;
			case 7:
				cell.textLabel.text = kOld8M;
				break;
			case 8:
				cell.textLabel.text = kOld9M;
				break;
			case 9:
				cell.textLabel.text = kOld10M;
				break;
			case 10:
				cell.textLabel.text = kOld11M;
				break;
			case 11:
				cell.textLabel.text = kOld12M;
				break;
			case 12:
				cell.textLabel.text = kOld13M;
				break;
			case 13:
				cell.textLabel.text = kOld14M;
				break;
			case 14:
				cell.textLabel.text = kOld15M;
				break;
			case 15:
				cell.textLabel.text = kOld16M;
				break;
			case 16:
				cell.textLabel.text = kOld17M;
				break;
			case 17:
				cell.textLabel.text = kOld18M;
				break;
			case 18:
				cell.textLabel.text = kOld19M;
				break;
			case 19:
				cell.textLabel.text = kOld20M;
				break;
			case 20:
				cell.textLabel.text = kOld21M;
				break;
			case 21:
				cell.textLabel.text = kOld22M;
				break;
			case 22:
				cell.textLabel.text = kOld23M;
				break;
			case 23:
				cell.textLabel.text = kOld24M;
				break;
			case 24:
				cell.textLabel.text = kOld25M;
				break;
			case 25:
				cell.textLabel.text = kOld26M;
				break;
			case 26:
				cell.textLabel.text = kOld27M;
				break;
			case 27:
				cell.textLabel.text = kOld28M;
				break;
			case 28:
				cell.textLabel.text = kOld29M;
				break;
			case 29:
				cell.textLabel.text = kOld30M;
				break;
			case 30:
				cell.textLabel.text = kOld31M;
				break;
			case 31:
				cell.textLabel.text = kOld32M;
				break;
			case 32:
				cell.textLabel.text = kOld33M;
				break;
			case 33:
				cell.textLabel.text = kOld34M;
				break;
			case 34:
				cell.textLabel.text = kOld35M;
				break;
			case 35:
				cell.textLabel.text = kOld36M;
				break;
			case 36:
				cell.textLabel.text = kOld37M;
				break;
			case 37:
				cell.textLabel.text = kOld38M;
				break;
			case 38:
				cell.textLabel.text = kOld39M;
				break;
			case 39:
				cell.textLabel.text = kOld40M;
				break;
			case 40:
				cell.textLabel.text = kOld41M;
				break;
			case 41:
				cell.textLabel.text = kOld42M;
				break;
			case 42:
				cell.textLabel.text = kOld43M;
				break;
			case 43:
				cell.textLabel.text = kOld44M;
				break;
			case 44:
				cell.textLabel.text = kOld45M;
				break;
			default:
				break;
		}
	}else if (indexPath.section == 1) {
		switch (indexPath.row) {
			case 0:
				cell.textLabel.text = kNew1M;
				break;
			case 1:
				cell.textLabel.text = kNew2M;
				break;
			case 2:
				cell.textLabel.text = kNew3M;
				break;
			case 3:
				cell.textLabel.text = kNew4M;
				break;
			case 4:
				cell.textLabel.text = kNew5M;
				break;
			case 5:
				cell.textLabel.text = kNew6M;
				break;
			case 6:
				cell.textLabel.text = kNew7M;
				break;
			case 7:
				cell.textLabel.text = kNew8M;
				break;
			case 8:
				cell.textLabel.text = kNew9M;
				break;
			case 9:
				cell.textLabel.text = kNew10M;
				break;
			case 10:
				cell.textLabel.text = kNew11M;
				break;
			case 11:
				cell.textLabel.text = kNew12M;
				break;
			case 12:
				cell.textLabel.text = kNew13M;
				break;
			case 13:
				cell.textLabel.text = kNew14M;
				break;
			case 14:
				cell.textLabel.text = kNew15M;
				break;
			case 15:
				cell.textLabel.text = kNew16M;
				break;
			case 16:
				cell.textLabel.text = kNew17M;
				break;
			case 17:
				cell.textLabel.text = kNew18M;
				break;
			case 18:
				cell.textLabel.text = kNew19M;
				break;
			case 19:
				cell.textLabel.text = kNew20M;
				break;
			case 20:
				cell.textLabel.text = kNew21M;
				break;
			case 21:
				cell.textLabel.text = kNew22M;
				break;
			case 22:
				cell.textLabel.text = kNew23M;
				break;
			case 23:
				cell.textLabel.text = kNew24M;
				break;
			case 24:
				cell.textLabel.text = kNew25M;
				break;
			case 25:
				cell.textLabel.text = kNew26M;
				break;
			case 26:
				cell.textLabel.text = kNew27M;
				break;
			default:
				break;
		}
	}
	cell.imageView.image = [UIImage imageNamed:@"cross"];
	cell.imageView.contentMode = UIViewContentModeRedraw;
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	UIStoryboard *mystoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
	HBChapterListViewController *HBCListVC = [mystoryboard instantiateViewControllerWithIdentifier:@"HBChapterListViewController"];
	[sBibleArray removeAllObjects];
	if (indexPath.section == 0) {
		switch (indexPath.row) {
			case 0:
				[self ManipulateBible:kOld1E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld1M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 1:
				[self ManipulateBible:kOld2E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld2M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 2:
				[self ManipulateBible:kOld3E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld3M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 3:
				[self ManipulateBible:kOld4E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld4M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 4:
				[self ManipulateBible:kOld5E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld5M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 5:
				[self ManipulateBible:kOld6E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld6M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 6:
				[self ManipulateBible:kOld7E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld7M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 7:
				[self ManipulateBible:kOld8E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld8M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 8:
				[self ManipulateBible:kOld9E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld9M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 9:
				[self ManipulateBible:kOld10E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld10M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 10:
				[self ManipulateBible:kOld11E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld11M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 11:
				[self ManipulateBible:kOld12E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld12M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 12:
				[self ManipulateBible:kOld13E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld13M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 13:
				[self ManipulateBible:kOld14E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld14M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 14:
				[self ManipulateBible:kOld15E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld15M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 15:
				[self ManipulateBible:kOld16E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld16M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 16:
				[self ManipulateBible:kOld17E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld17M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 17:
				[self ManipulateBible:kOld18E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld18M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 18:
				[self ManipulateBible:kOld19E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld19M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 19:
				[self ManipulateBible:kOld20E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld20M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 20:
				[self ManipulateBible:kOld21E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld21M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 21:
				[self ManipulateBible:kOld22E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld22M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 22:
				[self ManipulateBible:kOld23E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld23M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 23:
				[self ManipulateBible:kOld24E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld24M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 24:
				[self ManipulateBible:kOld25E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld25M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 25:
				[self ManipulateBible:kOld26E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld26M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 26:
				[self ManipulateBible:kOld27E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld27M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 27:
				[self ManipulateBible:kOld28E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld28M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 28:
				[self ManipulateBible:kOld29E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld29M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 29:
				[self ManipulateBible:kOld30E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld30M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 30:
				[self ManipulateBible:kOld31E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld31M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 31:
				[self ManipulateBible:kOld32E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld32M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 32:
				[self ManipulateBible:kOld33E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld33M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 33:
				[self ManipulateBible:kOld34E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld34M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 34:
				[self ManipulateBible:kOld35E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld35M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 35:
				[self ManipulateBible:kOld36E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld36M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 36:
				[self ManipulateBible:kOld37E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld37M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 37:
				[self ManipulateBible:kOld38E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld38M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 38:
				[self ManipulateBible:kOld39E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld39M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 39:
				[self ManipulateBible:kOld40E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld40M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 40:
				[self ManipulateBible:kOld41E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld41M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 41:
				[self ManipulateBible:kOld42E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld42M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 42:
				[self ManipulateBible:kOld43E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld43M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 43:
				[self ManipulateBible:kOld44E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld44M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 44:
				[self ManipulateBible:kOld45E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kOld45M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			default:
				break;
		}
	}else if (indexPath.section == 1) {
		switch (indexPath.row) {
			case 0:
				[self ManipulateBible:kNew1E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kNew1M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 1:
				[self ManipulateBible:kNew2E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kNew2M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 2:
				[self ManipulateBible:kNew3E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kNew3M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 3:
				[self ManipulateBible:kNew4E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kNew4M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 4:
				[self ManipulateBible:kNew5E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kNew5M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 5:
				[self ManipulateBible:kNew6E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kNew6M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 6:
				[self ManipulateBible:kNew7E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kNew7M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 7:
				[self ManipulateBible:kNew8E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kNew8M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 8:
				[self ManipulateBible:kNew9E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kNew9M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 9:
				[self ManipulateBible:kNew10E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kNew10M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 10:
				[self ManipulateBible:kNew11E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kNew11M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 11:
				[self ManipulateBible:kNew12E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kNew12M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 12:
				[self ManipulateBible:kNew13E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kNew13M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 13:
				[self ManipulateBible:kNew14E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kNew14M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 14:
				[self ManipulateBible:kNew15E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kNew15M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 15:
				[self ManipulateBible:kNew16E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kNew16M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 16:
				[self ManipulateBible:kNew17E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kNew17M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 17:
				[self ManipulateBible:kNew18E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kNew18M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 18:
				[self ManipulateBible:kNew19E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kNew19M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 19:
				[self ManipulateBible:kNew20E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kNew20M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 20:
				[self ManipulateBible:kNew21E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kNew21M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 21:
				[self ManipulateBible:kNew22E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kNew22M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 22:
				[self ManipulateBible:kNew23E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kNew23M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 23:
				[self ManipulateBible:kNew24E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kNew24M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 24:
				[self ManipulateBible:kNew25E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kNew25M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 25:
				[self ManipulateBible:kNew2E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kNew2M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			case 26:
				[self ManipulateBible:kNew27E];
				if ([sBibleArray count]) {
					HBCListVC.PassingHeaderText = kNew27M;
					HBCListVC.BibleArray = sBibleArray;
				}
				break;
			default:
				break;
		}
	}
	if (sBibleArray) {
		[self.navigationController pushViewController:HBCListVC animated:YES];
	}
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - ManipulateBible
- (void)ManipulateBible:(NSString *)fileName {
	NSError *error;
	NSString *strFileContent = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:fileName ofType: @"usfm"] encoding:NSUTF8StringEncoding error:&error];

	sBibleArray = [NSMutableArray arrayWithArray:[strFileContent componentsSeparatedByString:@"~~c"]];
	if (sBibleArray) {
		[sBibleArray removeObjectAtIndex:0];
	}
//	NSLog(@"File content : %@ ", strFileContent);
	NSLog(@"arrayCount: %lu", (unsigned long)[sBibleArray count]);
}

@end
