//
//  UIColor+GlobalColor.h
//  ChurchApp
//
//  Created by Grigo Mathews on 30/01/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (GlobalColor)

+ (UIColor *)headerBgColor;
+ (UIColor *)cardBgColor;

@end
