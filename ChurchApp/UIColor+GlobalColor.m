//
//  UIColor+GlobalColor.m
//  ChurchApp
//
//  Created by Grigo Mathews on 30/01/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import "UIColor+GlobalColor.h"

@implementation UIColor (GlobalColor)

+ (UIColor *)headerBgColor { // blue
	return [UIColor colorWithRed:14.0f/255.0f green:99.0f/255.0f blue:184.0f/255.0f alpha:1.0f];
}

+ (UIColor *)cardBgColor {
	return [UIColor colorWithRed:(250/255.0) green:(250/255.0) blue:(250/255.0) alpha:1];
}

@end
