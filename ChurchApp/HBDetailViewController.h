//
//  HBDetailViewController.h
//  ChurchApp
//
//  Created by Grigo Mathews on 13/02/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HBDetailViewController : UIViewController

@property (strong, nonatomic) NSString *passingTitle;
@property (strong, nonatomic) NSString *passingText;

@end
