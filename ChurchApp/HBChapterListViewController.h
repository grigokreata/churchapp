//
//  HBChapterListViewController.h
//  ChurchApp
//
//  Created by Grigo Mathews on 13/02/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HBChapterListViewController : UIViewController

@property (strong, nonatomic) NSString *PassingHeaderText;
@property (strong, nonatomic) NSMutableArray *BibleArray;

@end
