//
//  OfferingsViewController.h
//  ChurchApp
//
//  Created by Grigo Mathews on 31/01/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OfferingsDelegate <NSObject>

@optional

- (void)getSelectedArray:(NSMutableArray *)ratearray MalArray:(NSMutableArray *)malarray;

@end

@interface OfferingsViewController : UIViewController

@end
