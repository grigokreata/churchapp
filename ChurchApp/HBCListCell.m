//
//  HBCListCell.m
//  ChurchApp
//
//  Created by Grigo Mathews on 13/02/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import "HBCListCell.h"
#import "UIColor+GlobalColor.h"

@implementation HBCListCell
@synthesize CardView;

- (void)awakeFromNib {
	[super awakeFromNib];
	CardView.backgroundColor		= [UIColor cardBgColor];
	CardView.layer.shadowColor		= [UIColor grayColor].CGColor;
	CardView.layer.shadowOffset		= CGSizeMake(0, 1);
	CardView.layer.shadowOpacity	= 1;
	CardView.layer.shadowRadius		= 1.0;
	CardView.layer.cornerRadius		= 5.0;
	CardView.clipsToBounds			= NO;
}

@end
