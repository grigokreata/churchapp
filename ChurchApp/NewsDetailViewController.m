//
//  NewsDetailViewController.m
//  ChurchApp
//
//  Created by Grigo Mathews on 24/02/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import "NewsDetailViewController.h"
#import "NDImageCell.h"
#import "NDTextCell.h"
#import "UIImageView+WebCache.h"

@interface NewsDetailViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *NDTableView;
- (IBAction)BackBtnAction:(id)sender;
@end

@implementation NewsDetailViewController
@synthesize NDTableView;
@synthesize passingDic;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)BackBtnAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *cellId2 = @"NDTextCell";
	
	if (indexPath.row == 0) {
		return 150;
	}else {
		NDTextCell *textCell = (NDTextCell *)[tableView dequeueReusableCellWithIdentifier:cellId2];
		if(textCell == nil) {
			NSArray *customObjects = [[NSBundle mainBundle]loadNibNamed:@"NDTextCell" owner:self options:nil];
			for(id objects in customObjects){
				if([objects isKindOfClass:[UITableViewCell class]]) {
					textCell = (NDTextCell *)objects;
				}
			}
		}
		CGSize maximumLabelSize = CGSizeMake(296, 480);
		CGFloat width = maximumLabelSize.width;
		UIFont *font = textCell.NText.font;
		NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:[passingDic objectForKey:@"message"] attributes:@{NSFontAttributeName: font}];
		CGRect rect = [attributedText boundingRectWithSize:(CGSize){width, CGFLOAT_MAX} options:NSStringDrawingUsesLineFragmentOrigin  context:nil];
		CGSize size = rect.size;
		
		//adjust the label the the new height.
		CGRect newFrame = textCell.NText.frame;
		newFrame.size.height = size.height;
		textCell.NText.frame = newFrame;
		CGFloat height = newFrame.size.height+50;
		return height;
	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellId1 = @"NDImageCell";
    static NSString *cellId2 = @"NDTextCell";
    
    if (indexPath.row == 0) {
        NDImageCell *imageCell = (NDImageCell *)[tableView dequeueReusableCellWithIdentifier:cellId1];
        if(imageCell == nil) {
            NSArray *customObjects = [[NSBundle mainBundle]loadNibNamed:@"NDImageCell" owner:self options:nil];
            for(id objects in customObjects){
                if([objects isKindOfClass:[UITableViewCell class]]) {
                    imageCell = (NDImageCell *)objects;
                }
            }
        }
		NSURL *fimage = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[passingDic objectForKey:@"image"]]];
		[imageCell.NImage sd_setImageWithURL:fimage placeholderImage:[UIImage imageNamed:@"bg"]];
        return imageCell;
    }else {
        NDTextCell *textCell = (NDTextCell *)[tableView dequeueReusableCellWithIdentifier:cellId2];
        if(textCell == nil) {
            NSArray *customObjects = [[NSBundle mainBundle]loadNibNamed:@"NDTextCell" owner:self options:nil];
            for(id objects in customObjects){
                if([objects isKindOfClass:[UITableViewCell class]]) {
                    textCell = (NDTextCell *)objects;
                }
            }
        }
		textCell.NText.text = [passingDic objectForKey:@"message"];
        return textCell;
    }
}
@end
