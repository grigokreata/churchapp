//
//  GalleryViewController.h
//  ChurchApp
//
//  Created by Grigo Mathews on 31/01/17.
//  Copyright © 2017 Grigo Mathews. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CAPSGalleryViewController.h"

@interface GalleryViewController : UIViewController

@property (strong, nonatomic) id<CAPSGalleryDelegate> delegateGD;

@property long passingNo;

@end
